import { Component } from '@angular/core';

import {NavController, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Constants} from 'src/utils/constants';
import {AppStorage} from 'src/providers/data/appstorage';
import {Router} from '@angular/router';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  // rootPage:any = 'SlidesPage';
  driverId: any = null;
  userName: any = null;
  appID: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: AppStorage,
    public navCtrl: NavController,
    public firebaseX: FirebaseX,
    public router: Router
  ) {
    // this.statusBar.overlaysWebView(true);
    this.statusBar.backgroundColorByHexString('#ffffff');
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.firebaseX.getToken().then(token => {
            console.log('getToken---', token);
            console.log('registration ID app1---', token);
            this.appID = token;
            console.log('appID ID app2---', this.appID);
            this.storage.setValue(Constants.APPFCM_ID, this.appID);
          }).catch(error => console.error('Error getting token', error));
      this.firebaseX.onMessageReceived()
          .subscribe(data => console.log(`User opened a notification ${data}`));
      this.firebaseX.onTokenRefresh()
          .subscribe((token: string) => console.log(`Got a new token ${token}`));
      this.storage.getValue(Constants.USER_NAME).then(count => {
        this.userName= count;
        this.storage.getValue(Constants.USER_ID).then(count => {
          this.driverId= count;
          if(this.driverId != null && this.userName != null){
            this.navCtrl.navigateForward('/menu/home');
          }
          else{
            this.navCtrl.navigateForward('/login');
          }
        });
      });
    });
  }
}
