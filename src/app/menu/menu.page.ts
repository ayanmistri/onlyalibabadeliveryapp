import {Component, OnInit} from '@angular/core';
import {Router, RouterEvent} from '@angular/router';
import {Platform, MenuController, ToastController, LoadingController, AlertController} from '@ionic/angular';
import {AppStorage} from 'src/providers/data/appstorage';
import {Constants} from 'src/utils/constants';


@Component({
    selector: 'app-menu',
    templateUrl: './menu.page.html',
    styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
    selectedPath = '';
    userName: any;
    phNo: any;
    pages = [
        {
            title: 'Home',
            url: '/menu/home',
            icon: 'home'
        },
        {
            title: 'Order',
            url: '/menu/about',
            icon: 'document'
        },
        {
            title: 'Past Order History',
            url: '/menu/contact',
            icon: 'copy'
        },
        {
            title: 'My Profile',
            url: '/menu/more',
            icon: 'person'
        }

    ];

    constructor(public alertCtrl: AlertController, public  loadingCtrl: LoadingController, public toastCtrl: ToastController, public menuCtrl: MenuController, public storage: AppStorage, private  router: Router, public platform: Platform) {
        this.storage.getValue(Constants.USER_NAME).then(name => {
            this.userName = name;
        });
        this.storage.getValue(Constants.MOBILE).then(phno => {
            this.phNo = phno;
        });
        // this.router.events.subscribe((event: RouterEvent) => {
        //   if (event && event.url) {
        //     this.selectedPath = event.url;
        //   }
        // });
    }

    ngOnInit() {

    }

    async logOut() {
        this.menuCtrl.close();
        const alert = await this.alertCtrl.create({
            header: 'Log Out',
            message: 'Are you sure to logout?',
            buttons: [{
                text: "No"
            }, {
                text: "Yes",
                handler: () => {
                    this.storage.storage.clear();
                    this.router.navigate(['/login']);
                }
            }]
        });
        await alert.present();
    }
}