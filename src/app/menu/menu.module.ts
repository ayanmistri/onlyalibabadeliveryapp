import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {MenuPage} from './menu.page';

// const routes: Routes = [
//     {
//         path: '',
//         component: MenuPage,
//         children: [
//             {
//                 path: 'dashboard',
//                 loadChildren: '../dashboard/dashboard.module#DashboardPageModule',
//
//             }
//             // {
//             //   path: 'cart',
//             //   loadChildren: '../cart/cart.module#CartPageModule',
//             //
//             // }
//
//         ]
//     }
// ];

const routes: Routes = [
    {
        path: '',
        component: MenuPage,
        children: [
            {
                path: 'home',
                loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
            },
            {
                path: 'about',
                loadChildren: () => import('../about/about.module').then( m => m.AboutPageModule)
            },
            {
                path: 'contact',
                loadChildren: () => import('../contact/contact.module').then( m => m.ContactPageModule)
            },
            {
                path: 'more',
                loadChildren: () => import('../more/more.module').then( m => m.MorePageModule)
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [MenuPage]
})
export class MenuPageModule {
}
