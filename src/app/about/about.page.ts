import {Component, ViewChild, Directive, Renderer, ElementRef, OnInit} from '@angular/core';
import {
  NavController,
  LoadingController,
  Platform,
  ModalController,
  ActionSheetController,
  IonRouterOutlet
} from '@ionic/angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {MessageHelper} from 'src/providers/message-helper';
import {AppStorage} from 'src/providers/data/appstorage';
import {Constants} from 'src/utils/constants';
import {HttpProvider} from 'src/providers/data/data';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  userID: any;
  allOrderlists: any = [];
  private start: number = 1;
  hasMoreData: boolean = true;
  passValues = false;
  constructor(public navCtrl: NavController, public msgHelper: MessageHelper, public loadingCtrl: LoadingController, public platform: Platform, public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController, public storage: AppStorage, public formBuilder: FormBuilder, private  router: Router, public modalController: ModalController, public element: ElementRef, public renderer: Renderer, public http: HttpProvider, public geolocation: Geolocation) {
    this.storage.getValue(Constants.USER_ID).then(userid => {
      this.userID = userid;
      console.log('orderPage userID', this.userID);
      this.getAllOrders();
      this.start = 1;
    });
  }

  ngOnInit() {
  }
  orderDetails(order) {
    this.router.navigate(['/product-details', {orderNo: order.OrderNo}]);
  }
  getAllOrders() {
    this.http.callApi('pendingorderlist', {
      'DriverId': this.userID,
      'OrderNo': '',
      'CustomerName': '',
      'CustomerPhone': '',
      'Page': '1',
      'Size': '10'
    }).then((data: any) => {
      if (data.status == Constants.SUCCESS) {
        if (!this.http.isNullOrEmpty(data)) {
          this.allOrderlists = data.data;
          console.log('allOrderlists 1st 10--', this.allOrderlists);
          this.passValues = true;
        }
      } else {
        this.msgHelper.presentAlertok('Failed', 'Please try again.');
      }
    });
  }


  doInfinite(): Promise<any> {
    this.start += 1;
    // this.pageSize += 10;
    return new Promise((resolve) => {
      this.http.callApi('pendingorderlist', {
        'DriverId': this.userID,
        'OrderNo': '',
        'CustomerName': '',
        'CustomerPhone': '',
        'Page': this.start,
        'Size': '10'
      }).then((data: any) => {
        //loader.dismiss();
        let count = 0;
        if (!this.http.isNullOrEmpty(data)) {
          console.log(data.data);
          this.allOrderlists = [].concat(this.allOrderlists, data.data);
          console.log('allOrderlists 2nd--', this.allOrderlists);

        }
        if (count > 0) {
          resolve();
        } else {
          this.hasMoreData = false;
        }
      }, (err) => {
        console.log(err);

        resolve();
      });
    });
  }
}
