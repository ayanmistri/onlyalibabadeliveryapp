import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)},
    {path: 'home', loadChildren: './home/home.module#HomePageModule'},
    {path: 'menu', loadChildren: './menu/menu.module#MenuPageModule'},
    {path: 'about', loadChildren: './about/about.module#AboutPageModule'}, // Order page
    {path: 'contact', loadChildren: './contact/contact.module#ContactPageModule'}, // Past Order History page
    {path: 'more', loadChildren: './more/more.module#MorePageModule'}, // My Profile page
    {path: 'product-details', loadChildren: './product-details/product-details.module#ProductDetailsPageModule'},
    { path: 'registration', loadChildren: './registration/registration.module#RegistrationPageModule' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
