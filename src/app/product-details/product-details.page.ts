import {Component, ViewChild, Directive, Renderer, ElementRef, OnInit} from '@angular/core';
import {
    NavController,
    LoadingController,
    Platform,
    ModalController,
    ActionSheetController,
    IonRouterOutlet, AlertController
} from '@ionic/angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageHelper} from 'src/providers/message-helper';
import {AppStorage} from 'src/providers/data/appstorage';
import {Constants} from 'src/utils/constants';
import {HttpProvider} from 'src/providers/data/data';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {HttpClient, HttpHeaders} from '@angular/common/http';
declare var google;
@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.page.html',
    styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
    orderNo: any;
    userID: any;
    userAddress: any;
    userAddressName: any;
    userLatitude: any;
    userLongitude: any;
    userOrderDate: any;
    userOrderDetail: any;
    userOrderNo: any;
    userOrderTime: any;
    userOrderTotal: any;
    userPincode: any;
    userPhone: any;
    userCusName: any;
    userPaymentMode: any;
    userCustomerId: any;
    userRefCustomerId: any;
    mapProduct: any;
    datas: any;
    area: any;
    gpsaddress: any;
    lat: any;
    lon: any;

    constructor(public navCtrl: NavController, public msgHelper: MessageHelper, public loadingCtrl: LoadingController, public platform: Platform, public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController, public storage: AppStorage, public formBuilder: FormBuilder, private  router: Router, public renderer: Renderer, public http: HttpProvider, public geolocation: Geolocation, public activeRouter: ActivatedRoute, public callNumber: CallNumber, public alertController: AlertController, public httpp: HttpClient) {
        this.orderNo = this.activeRouter.snapshot.paramMap.get('orderNo') as string;
        this.storage.getValue(Constants.USER_ID).then(userid => {
            this.userID = userid;
            console.log('ProductDetailsPage userID', this.userID);
            this.getOrderDetails();
        });
    }

    ngOnInit() {
    }

    getOrderDetails() {
        this.msgHelper.presentLoading();
        this.platform.ready().then(() => {
            this.http.callApi('orderdetailbyid', {
                'OrderId': this.orderNo,
            }).then((data: any) => {
                this.msgHelper.dismiss();
                if (data.status == Constants.SUCCESS) {
                    this.userAddress = data.data[0].Address;
                    this.userAddressName = data.data[0].AddressName;
                    this.userLatitude = data.data[0].Latitude;
                    this.userLongitude = data.data[0].Longitude;
                    this.userOrderDate = data.data[0].OrderDate;
                    this.userOrderDetail = data.data[0].OrderDetail;
                    this.userOrderNo = data.data[0].OrderNo;
                    this.userOrderTime = data.data[0].OrderTime;
                    this.userOrderTotal = data.data[0].OrderTotal;
                    this.userPincode = data.data[0].Pincode;
                    this.userCusName = data.data[0].Name;
                    this.userPhone = data.data[0].Phone;
                    this.userPaymentMode = data.data[0].PaymentMode;
                    this.userCustomerId = data.data[0].CustomerId;
                    this.userRefCustomerId = data.data[0].RefCustomerId;
                    var lat = this.userLatitude;
                    var long = this.userLongitude
                    var x: number = +this.userLatitude;
                    var y: number = +this.userLongitude;
                    const uluru = {lat: x, lng: y};
                    console.log('lat-long--', uluru);
                    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
                    var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + this.userLatitude + ',' + this.userLongitude + '&key=AIzaSyDSPMOX4Mr6bbq7Zkomw405OpYaC4CxAsg';
                    console.log(url);
                    this.httpp.get(url, {headers, responseType: 'text'}).subscribe(data => {
                        this.datas = JSON.parse(data);
                        console.log('data:', this.datas);
                        let address = this.datas.results[0].formatted_address;
                        this.area = this.datas.results[0].address_components[1].short_name;
                        console.log('My location:', address);
                        this.gpsaddress = address;
                    })
                    this.platform.ready().then(() => {
                        this.geolocation.getCurrentPosition().then((resp) => {
                            this.lat = resp.coords.latitude;
                            this.lon = resp.coords.longitude;
                            console.log('my latitude --', this.lat);
                            console.log('my longitude --', this.lon);
                            const uluruss = {lat: this.lat, lng: this.lon};
                            this.mapProduct = new google.maps.Map(document.getElementById('mapProduct'), {
                                zoom: 9,
                                center: uluruss,
                                disableDefaultUI: true,
                            })
                            var marker = new google.maps.Marker({
                                position: uluruss,
                                map: this.mapProduct,
                                label: {
                                    fontSize: "10pt",
                                    text: "You are here",
                                    fontWeight: "700",
                                    color: "#f00"
                                },
                                icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
                            });
                            var marker1 = new google.maps.Marker({
                                position: uluru,
                                map: this.mapProduct,
                                label: {
                                    fontSize: "10pt",
                                    text: "Delivery Location",
                                    fontWeight: "700",
                                    color: "#f00"
                                },
                                icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                            });
                        }).catch((error) => {
                            console.log('Error getting location', error);
                            // this.msgHelper.presentAlertok('Failed...', 'Do not fetch your location please check your gps connection.');
                        });
                    });
                    ////////////////////////////////
                } else {
                    this.msgHelper.presentAlertok('Failed', 'Please try again.');
                }
            });
        });
    }

    viewPh(userPhone) {
        console.log('userPhone---', userPhone);
        this.callNumber.callNumber(userPhone, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

    viewMapOn() {
        if (this.platform.is('android')) {
            // window.location.href = 'geo:,' + uluru;
            window.location.href = 'geo:' + '?q=' + this.gpsaddress;
        } else {
            var x: number = +this.userLatitude;
            var y: number = +this.userLongitude;
            const destination = {lat: x, lng: y};
            // const destination = '51.503391' + ',' + '-0.127630';
            window.open('maps://?q=' + destination + '_system');
            // window.location.href = 'maps://maps.apple.com/?q=' +  uluru;
        }
    }

    async viewDelivery() {
        const alert = await this.alertController.create({
            header: 'OTP',
            message: 'Please enter the OTP sent to the customer order details.',
            inputs: [
                {
                    name: 'otp',
                    placeholder: 'OTP',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'OK',
                    handler: data => {
                        if (data.otp) {
                            this.msgHelper.presentLoading();
                            this.http.callApi('orderupdate', {
                                DriverId: this.userID,
                                OrderId: this.userOrderNo,
                                CustomerId: this.userCustomerId,
                                RefCustomerId: this.userRefCustomerId,
                                OrderTotal: this.userOrderTotal,
                                OTP: data.otp
                            }).then((data: any) => {
                                this.msgHelper.dismiss();
                                if (data.status == Constants.SUCCESS) {
                                    this.msgHelper.presentToast('Your order delivery has been completed.');
                                    this.router.navigate(['/menu/about']);
                                } else {
                                    this.msgHelper.presentAlertok('Wrong OTP', 'The OTP you have entered is wrong. Please try again.');
                                    return false;
                                }
                            });
                        } else {
                            return false;
                        }
                    }
                }
            ]
        });

        alert.present();
        let result = alert.onDidDismiss();
        console.log(result);
    }

}
