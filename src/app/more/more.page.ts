import {Component, OnInit} from '@angular/core';
import {LoadingController, AlertController} from '@ionic/angular';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageHelper} from 'src/providers/message-helper';
import {AppStorage} from 'src/providers/data/appstorage';
import {Constants} from 'src/utils/constants';
import {HttpProvider} from 'src/providers/data/data';
import {NavController} from '@ionic/angular';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
@Component({
    selector: 'app-more',
    templateUrl: './more.page.html',
    styleUrls: ['./more.page.scss'],
})
export class MorePage implements OnInit {
    contactForm: FormGroup;
    base64Image: any = null;
    base64ImageNew: any;

    submitAttempt: boolean = false;
    fullNameChanged: boolean = false;
    emailChanged: boolean = false;
    addressChanged: boolean = false;
    landmarkChanged: boolean = false;
    pincodeChanged: boolean = false;

    constructor(public alertController: AlertController, public loadingController: LoadingController, private router: Router, public formBuilder: FormBuilder,
                public msgHelper: MessageHelper, public storage: AppStorage, public http: HttpProvider, public navCtrl: NavController, public camera: Camera) {
        this.contactForm = this.formBuilder.group({
            fullName: new FormControl('', [Validators.required, Validators.minLength(8)]),
            email: new FormControl('', [Validators.required, Validators.pattern(Constants.EMAIL_REGEX)]),
            // phone: new FormControl ('', [Validators.required, Validators.minLength(10)]),
            address: new FormControl('', [Validators.required, Validators.minLength(10)]),
            landmark: new FormControl('', [Validators.required]),
            pincode: new FormControl('', [Validators.pattern(Constants.PIN_REGEX)])
        });
        this.storage.getValue(Constants.USER_NAME).then(username => {
            this.storage.getValue(Constants.EMAIL).then(email => {
                // this.storage.getValue(Constants.MOBILE).then(phone => {
                this.storage.getValue(Constants.ADDRESS).then(address => {
                    this.storage.getValue(Constants.Landmark).then(landmark => {
                        this.storage.getValue(Constants.PINCODE).then(pincode => {
                            this.contactForm = this.formBuilder.group({
                                fullName: new FormControl(username, [Validators.required, Validators.minLength(8)]),
                                email: new FormControl(email, [Validators.required, Validators.pattern(Constants.EMAIL_REGEX)]),
                                // phone:  new FormControl (phone, [Validators.required, Validators.minLength(10)]),
                                address: new FormControl(address, [Validators.required, Validators.minLength(10)]),
                                landmark: new FormControl(landmark, [Validators.required]),
                                pincode: new FormControl(pincode, [Validators.pattern(Constants.PIN_REGEX)])
                            });
                        });
                    });
                });
                // });
            });
        });
        this.storage.getValue(Constants.IMAGE).then(userimg => {
            this.base64ImageNew = userimg;
        });

    }

    ngOnInit() {
    }


    async openActionSheet() {
    const actionSheet = document.createElement('ion-action-sheet');

    actionSheet.header = 'Please select';
    actionSheet.cssClass = 'my-custom-class';
    actionSheet.buttons = [{
        text: 'Camera',
        // role: 'destructive',
        icon: 'camera',
        handler: () => {
            this.takePhoto();
        }
    }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
            this.openGallery();
        }
    }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
            console.log('Cancel clicked');
        }
    }];
    document.body.appendChild(actionSheet);
    return actionSheet.present();
}

    takePhoto() {
        const cameraOptions: CameraOptions = {
            quality: 50,
            targetHeight: 350,
            targetWidth: 350,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation : true,
            cameraDirection : 1
        };
        this.camera.getPicture(cameraOptions).then((imageData) => {
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log("img path", this.base64Image);
            this.base64ImageNew = imageData;
        }, (err) => {
            console.log(err);
        });

    }

    openGallery() {
        this.camera.getPicture({
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL
        }).then((imageData) => {
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log("img path", this.base64Image);
            this.base64ImageNew = imageData;
        }, (err) => {
            console.log(err);
        });
    }

    profileSave() {
            this.msgHelper.presentLoading();
            this.storage.getValue(Constants.USER_ID).then(driverId => {
                this.http.callApi("profileupdate", {
                    "driverId": driverId,
                    "name": this.contactForm.value.fullName,
                    "email": this.contactForm.value.email,
                    "address": this.contactForm.value.address,
                    "landmark": this.contactForm.value.landmark,
                    "pincode": this.contactForm.value.pincode,
                    "myFile": this.base64Image
                }).then((data: any) => {
                    this.msgHelper.dismiss();
                    if (data.status === Constants.SUCCESS) {
                        this.router.navigate(['/menu/home']);
                        this.msgHelper.presentToast("Profile updated successfully");
                    } else {
                        this.msgHelper.presentAlert("Failed", "Please try again.");
                    }

                });
            });
    }
}
