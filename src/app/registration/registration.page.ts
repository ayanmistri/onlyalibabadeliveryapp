import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { NavController, LoadingController, Platform, ModalController, ActionSheetController, AlertController, ToastController } from '@ionic/angular';
import {MessageHelper} from 'src/providers/message-helper';
import {FormBuilder, Validators, FormControl, FormGroup} from '@angular/forms';
import {AppStorage} from 'src/providers/data/appstorage';
import {HttpProvider} from 'src/providers/data/data';
import {Constants} from 'src/utils/constants';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

declare var SMSReceive: any;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  otpForm: FormGroup;
  mobile: any;
  appID: any;
  otp: any;
  finalotp: any;
  customerId: any;
  OTP: string = '';
  latitude: any;
  longitude: any;
  showOTPInput: boolean = false;
  OTPmessage: string = 'An OTP is sent to your number. You should receive it in 15 s';
  // devicekey: string = "bsdhhjbhjsfhhvshgvhgs";
  constructor(private  router: Router, public navCtrl: NavController, public msgHelper: MessageHelper, public loadingCtrl: LoadingController, public platform: Platform, public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController, public formBuilder: FormBuilder, private http: HttpProvider, public storage: AppStorage,public geolocation: Geolocation, public alertCtrl: AlertController, public toastCtrl: ToastController, public androidPermissions: AndroidPermissions, public activeRouter: ActivatedRoute) {
    this.otp = this.activeRouter.snapshot.paramMap.get('OTP') as string;
    this.checkPermissionAndSend();
    this.storage.getValue(Constants.MOBILE).then(phno => {
      this.mobile = phno;
    });
    this.storage.getValue(Constants.APPFCM_ID).then(fcmid => {
      this.appID = fcmid;
      console.log('RegistrationPage appID-', this.appID);
    });
    this.geolocation.getCurrentPosition().then((resp)=>{
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      console.log('latitude',this.latitude);
      this.storage.setValue(Constants.PlaceLatitude , this.latitude);
      this.storage.setValue(Constants.PlaceLongitude ,this.longitude);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    this.otpForm = formBuilder.group({
      one: new FormControl('', Validators.required),
      two: new FormControl('', Validators.required),
      three: new FormControl('', Validators.required),
      four: new FormControl('', Validators.required),
      five: new FormControl('', Validators.required),
      six: new FormControl('', Validators.required)
    });
  }
  checkPermissionAndSend() {

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.SEND_SMS).then(
        success => {
          if (!success.hasPermission) {
            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS).
            then((success) => {
                  this.start();
                },
                (err) => {
                  console.error(err);
                });
          } else {
            this.start();
          }
        },
        err => {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS).
          then((success) => {
                this.start();
              },
              (err) => {
                console.error(err);
              });
        });
  }

  // ionViewWillEnter() {
  //
  //   this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
  //       success => console.log('Permission granted'),
  //       err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
  //   );
  //
  //   this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
  // }
  // ionViewDidEnter() {
  //   this.platform.ready().then((readySource) => {
  //     if(SMS) SMS.startWatch(()=>{
  //       console.log('watching started');
  //     }, Error=>{
  //       console.log('failed to start watching');
  //     });
  //
  //     document.addEventListener('onSMSArrive', (e:any)=>{
  //       var sms = e.data;
  //       console.log(sms);
  //
  //     });
  //
  //   });
  // }


  ngOnInit() {
  }

  moveFocus(nextElement) {
    nextElement.setFocus();
  }

  goLogin() {
    this.router.navigate(['/login']);
  }

  async presentToast(message, show_button, position, duration) {
    const toast = await this.toastCtrl.create({
      message: message,
      showCloseButton: show_button,
      position: position,
      duration: duration
    });
    toast.present();
  }

  next() {
    this.showOTPInput = true;
    this.start();
  }

  start() {
    SMSReceive.startWatch(
        () => {
          document.addEventListener('onSMSArrive', (e: any) => {
            var IncomingSMS = e.data;
            this.processSMS(IncomingSMS);
          });
        },
        () => { console.log('watch start failed') }
    )
  }

  stop() {
    SMSReceive.stopWatch(
        () => { console.log('watch stopped') },
        () => { console.log('watch stop failed') }
    )
  }

  processSMS(data) {
    // Check SMS for a specific string sequence to identify it is you SMS
    // Design your SMS in a way so you can identify the OTP quickly i.e. first 6 letters
    // In this case, I am keeping the first 6 letters as OTP
    const message = data.body;
    if (message && message.indexOf('enappd_starters') != -1) {
      this.OTP = data.body.slice(0, 6);
      console.log('read OTP---', this.OTP);
      this.OTPmessage = 'OTP received. Proceed to register'
      this.stop();
    }
  }

  register() {
    if (this.OTP != '') {
      this.presentToast('You are successfully registered', false, 'top', 1500);
    } else {
      this.presentToast('Your OTP is not valid', false, 'bottom', 1500);
    }
  }


  signUp() {
    this.finalotp = this.otpForm.value.one + this.otpForm.value.two + this.otpForm.value.three + this.otpForm.value.four + this.otpForm.value.five + this.otpForm.value.six;
    this.storage.getValue(Constants.USER_ID).then(userid => {
      this.msgHelper.presentLoading();
      this.http.callApi("drivervalidate", {
        DriverId: userid,
        OTP: this.finalotp
      }).then((data: any) => {
        this.msgHelper.dismiss();
        if (data.status == Constants.SUCCESS) {
          this.msgHelper.presentToast("Login successfully");
          this.storage.setValue(Constants.USER_ID, data.data[0].DriverId);
          this.storage.setValue(Constants.USER_NAME, data.data[0].Name);
          this.storage.setValue(Constants.MOBILE, data.data[0].Phone);
          this.storage.setValue(Constants.EMAIL, data.data[0].Email);
          this.storage.setValue(Constants.ADDRESS, data.data[0].Address);
          this.storage.setValue(Constants.PINCODE, data.data[0].Pincode);
          this.storage.setValue(Constants.Landmark, data.data[0].Landmark);
          this.storage.setValue(Constants.IMAGE, data.data[0].ProfileImage);
          this.storage.setValue(Constants.USER_Online, data.data[0].IsOnline);
          this.navCtrl.navigateForward('/menu/home');
        }
        else if(data.status == Constants.HTTP_ERROR){
          this.msgHelper.presentToast(data.message);
        }
      }).catch(error => {
        this.msgHelper.presentToast('Please try again..');
      });
    });
  }

  resend() {
    this.storage.getValue(Constants.MOBILE).then(phno => {
      this.msgHelper.presentLoading();
      this.http.callApiLogin("login", {
        "phnumber": phno,
        "devicekey": this.appID
      }).then((data: any) => {
        console.log('data', data.data);
        this.msgHelper.dismiss();
        if (data.status == Constants.SUCCESS) {
          this.otp = data.data[0].OTP;
          console.log('otp', this.otp);
          this.otpForm.reset();
          this.msgHelper.presentToast("OTP Sent!");
        }
        else {
          this.msgHelper.presentToast("Failed");
        }
      }).catch(error => {
        this.msgHelper.presentToast('Please try again..');
      });
      console.log("Mobile Number  : ", this.mobile);
    });
  }

}
