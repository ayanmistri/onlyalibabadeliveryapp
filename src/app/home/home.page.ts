import {Component, ViewChild, Directive, Renderer, ElementRef, OnInit} from '@angular/core';
import {
    NavController,
    LoadingController,
    Platform,
    ModalController,
    ActionSheetController,
    IonRouterOutlet
} from '@ionic/angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {MessageHelper} from 'src/providers/message-helper';
import {AppStorage} from 'src/providers/data/appstorage';
import {Constants} from 'src/utils/constants';
import {HttpProvider} from 'src/providers/data/data';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {LocationAccuracy} from '@ionic-native/location-accuracy/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

declare var google;
@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    togglevalue: any;
    toggleStatus: any;
    isToggled: boolean;
    name: any;
    userID: any;
    mobile: any;
    allOrderlists: any = [];
    private start: number = 1;
    private pageSize: number = 10;
    hasMoreData: boolean = true;
    lat: any;
    lon: any;
    @ViewChild(IonRouterOutlet, { static: false}) routerOutlet: IonRouterOutlet;
    map: any;
    passValues = false;
    locationCoords: any;
    dutyStatus: any;
    constructor(public navCtrl: NavController, public msgHelper: MessageHelper, public loadingCtrl: LoadingController, public platform: Platform, public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController, public storage: AppStorage, public formBuilder: FormBuilder, private  router: Router, public modalController: ModalController, public element: ElementRef, public renderer: Renderer, public http: HttpProvider, public geolocation: Geolocation, public locationAccuracy: LocationAccuracy, public androidPermissions: AndroidPermissions) {
        this.isToggled = true;
        this.platform.ready().then(() => {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.lat = resp.coords.latitude;
            this.lon = resp.coords.longitude;
            console.log('latitude --', this.lat);
            console.log('longitude --', this.lon);
            const uluru = {lat: this.lat, lng: this.lon};
            this.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: uluru,
                disableDefaultUI: true,
            })
            var marker = new google.maps.Marker({
                position: uluru,
                map: this.map,
                icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
            });
        }).catch((error) => {
            console.log('Error getting location', error);
            // this.msgHelper.presentAlertok('Failed...', 'Do not fetch your location please check your gps connection.');
        });
        });
        this.platform.backButton.subscribeWithPriority(0, () => {
            if (this.routerOutlet && this.routerOutlet.canGoBack()) {
                this.routerOutlet.pop();
            } else if (this.router.url === '/login') {
                navigator['app'].exitApp();
            }
            else if (this.router.url === '/menu/home') {
                navigator['app'].exitApp();
            }
            else {
                // this.msgHelper.presentToast("Exit", "Do you want to exit the app?", this.onYesHandler, this.onNoHandler, "backPress");
                // this.exitApp();
                this.router.navigate(['/menu/home']);
                this.msgHelper.presentToast('Please press again to exit app');
            }
        });
        this.storage.getValue(Constants.USER_ID).then(userid => {
            this.userID = userid;
            console.log('HomePage userID', this.userID);
        });
        this.storage.getValue(Constants.USER_NAME).then(name => {
            this.name = name;
            console.log('HomePage userName', this.name);
        });
        this.storage.getValue(Constants.MOBILE).then(phno => {
            this.mobile = phno;
            console.log('HomePage mobile', this.mobile);
        });
        this.getUserData();
    }

    //Check if application having GPS access permission
    checkGPSPermission() {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
            result => {
                if (result.hasPermission) {

                    //If having permission show 'Turn On GPS' dialogue
                    this.askToTurnOnGPS();
                } else {

                    //If not having permission ask for permission
                    this.requestGPSPermission();
                }
            },
            err => {
                // alert(err);
            }
        );
    }

    requestGPSPermission() {
        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if (canRequest) {
                console.log("4");
            } else {
                //Show 'GPS Permission Request' dialogue
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(
                        () => {
                            // call method to turn on GPS
                            this.askToTurnOnGPS();
                        },
                        error => {
                            //Show alert if user click on 'No Thanks'
                            // alert('requestPermission Error requesting location permissions ' + error);
                        }
                    );
            }
        });
    }

    askToTurnOnGPS() {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => {
                // When GPS Turned ON call method to get Accurate location coordinates
                this.getLocationCoordinates();
            },
            // error => alert('Error requesting location permissions ' + JSON.stringify(error))
        );
    }

    // Methos to get device accurate coordinates using device GPS
    getLocationCoordinates() {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.lat = resp.coords.latitude;
            this.lon = resp.coords.longitude;
            this.mapLoad();
        }).catch((error) => {
            console.log('Error getting location', error);
        });
    }

    ngOnInit() {
    }
    ionViewWillEnter() {
        this.checkGPSPermission();
        // this.mapLoad();
        // this.getUserData();
        this.getAllOrders();
        this.start = 1;
    }
    mapLoad(){
        this.geolocation.getCurrentPosition().then((resp) => {
            this.lat = resp.coords.latitude;
            this.lon = resp.coords.longitude;
            console.log('latitude --', this.lat);
            console.log('longitude --', this.lon);
            const uluru = {lat: this.lat, lng: this.lon};
            this.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: uluru,
                disableDefaultUI: true,
            })
            var marker = new google.maps.Marker({
                position: uluru,
                map: this.map,
                icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
            });
        }).catch((error) => {
            console.log('Error getting location', error);
            // this.msgHelper.presentAlertok('Failed...', 'Do not fetch your location please check your gps connection.');
        });
    }
    getUserData() {
        this.storage.getValue(Constants.USER_ID).then(userid => {
        this.platform.ready().then(() => {
            this.http.callApi('driverdetails', {
                'DriverId': userid,
            }).then((data: any) => {
                if (data.status == Constants.SUCCESS) {
                    this.storage.setValue(Constants.USER_NAME, data.data[0].Name);
                    this.storage.setValue(Constants.MOBILE, data.data[0].Phone);
                    this.storage.setValue(Constants.EMAIL, data.data[0].Email);
                    this.storage.setValue(Constants.ADDRESS, data.data[0].Address);
                    this.storage.setValue(Constants.PINCODE, data.data[0].Pincode);
                    this.storage.setValue(Constants.Landmark, data.data[0].Landmark);
                    this.storage.setValue(Constants.IMAGE, data.data[0].ProfileImage);
                    this.storage.setValue(Constants.USER_Online, data.data[0].IsOnline);
                    this.storage.getValue(Constants.USER_Online).then(togglestatus => {
                        this.toggleStatus = togglestatus;
                        console.log('HomePage toggleStatus---', this.toggleStatus);
                    });
                } else {
                    this.msgHelper.presentAlertok('Failed', 'Please try again.');
                }
            });
        });
        });
    }

    getAllOrders() {
        // this.msgHelper.presentLoading();
        this.storage.getValue(Constants.USER_ID).then(userid => {
        this.http.callApi('pendingorderlist', {
            'DriverId': userid,
            'OrderNo': '',
            'CustomerName': '',
            'CustomerPhone': '',
            'Page': '1',
            'Size': '10'
        }).then((data: any) => {
            // this.msgHelper.dismiss();
            if (data.status == Constants.SUCCESS) {
                if (!this.http.isNullOrEmpty(data)) {
                    this.allOrderlists = data.data;
                    console.log('allOrderlists 1st 10--', this.allOrderlists);
                    this.passValues = true;
                }
            } else {
                this.msgHelper.presentAlertok('Failed', 'Please try again.');
            }
        });
        });
    }


    doInfinite(): Promise<any> {
        this.start += 1;
        // this.pageSize += 10;
        return new Promise((resolve) => {
            this.http.callApi('pendingorderlist', {
                'DriverId': this.userID,
                'OrderNo': '',
                'CustomerName': '',
                'CustomerPhone': '',
                'Page': this.start,
                'Size': '10'
            }).then((data: any) => {
                let count = 0;
                if (!this.http.isNullOrEmpty(data)) {
                    console.log(data.data);
                    this.allOrderlists = [].concat(this.allOrderlists, data.data);
                    console.log('allOrderlists 2nd--', this.allOrderlists);

                }
                if (count > 0) {
                    resolve();
                } else {
                    this.hasMoreData = false;
                }
            }, (err) => {
                console.log(err);

                resolve();
            });
        });
    }

    myChange() {
        if (this.toggleStatus == 'true') {
            this.msgHelper.presentLoading();
            this.http.callApi('driverstatusupdate', {
                'IsOnline': 'false',
                'DriverId': this.userID
            }).then((data: any) => {
                this.msgHelper.dismiss();
                if (data.status == Constants.SUCCESS) {
                    this.getUserData();
                    // this.dutyStatus == "On Duty";
                    this.msgHelper.presentToast('You are offline...');
                    this.storage.setValue(Constants.USER_Online, 'false');
                } else {
                    this.msgHelper.presentAlertok('Failed', 'Please try again.');
                }
            });
        }
        else if (this.toggleStatus == 'false') {
            this.msgHelper.presentLoading();
            this.http.callApi('driverstatusupdate', {
                'IsOnline': 'true',
                'DriverId': this.userID
            }).then((data: any) => {
                this.msgHelper.dismiss();
                if (data.status == Constants.SUCCESS) {
                    this.getUserData();
                    // this.dutyStatus == "On Duty";
                    this.msgHelper.presentToast('You are online...');
                    this.storage.setValue(Constants.USER_Online, 'true');
                } else {
                    this.msgHelper.presentAlertok('Failed', 'Please try again.');
                }
            });
        }
    }
    orderDetails(order) {
        this.router.navigate(['/product-details', {orderNo: order.OrderNo}]);
    }

    doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        this.getAllOrders();
        this.getUserData();
        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }

}
