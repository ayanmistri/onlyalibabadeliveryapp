import {Component, OnInit, ViewChild} from '@angular/core';
import {LoadingController, AlertController, IonRouterOutlet, Platform} from '@ionic/angular';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageHelper} from 'src/providers/message-helper';
import {AppStorage} from 'src/providers/data/appstorage';
import {Constants} from 'src/utils/constants';
import {HttpProvider} from 'src/providers/data/data';
import {NavController} from '@ionic/angular';
import {LocationAccuracy} from '@ionic-native/location-accuracy/ngx';
declare var cordova;
@Component({
    selector: 'app-login',
    templateUrl: 'login.page.html',
    styleUrls: ['login.page.scss']
})
export class LoginPage implements OnInit {

    loginForm: FormGroup;
    loading: any;
    appID: any;
    // devicekey: string = "bsdhhjbhjsfhhvshgvhgs";
    @ViewChild(IonRouterOutlet, { static: false}) routerOutlet: IonRouterOutlet;
    constructor(public alertController: AlertController, public loadingController: LoadingController, private router: Router, public formBuilder: FormBuilder, public msgHelper: MessageHelper, public storage: AppStorage, public http: HttpProvider, public navCtrl: NavController, public platform: Platform, public locationAccuracy: LocationAccuracy) {
        this.loginForm = formBuilder.group({
            mobileno: ['', [Validators.required, Validators.minLength(10)]]
        });
        this.storage.getValue(Constants.APPFCM_ID).then(fcmid => {
            this.appID = fcmid;
            console.log('LoginPage appID-', this.appID);
        });
        this.platform.backButton.subscribeWithPriority(0, () => {
            if (this.routerOutlet && this.routerOutlet.canGoBack()) {
                this.routerOutlet.pop();
            } else if (this.router.url === '/login') {
                navigator['app'].exitApp();
            }
            else {
                // this.msgHelper.presentToast("Exit", "Do you want to exit the app?", this.onYesHandler, this.onNoHandler, "backPress");
                // this.exitApp();
                this.router.navigate(['/login']);
                // this.msgHelper.presentToast('Please press again to exit app');
            }
        });
    }

    ngOnInit() {

    }

    signIn() {
        this.msgHelper.presentLoading();
        this.storage.getValue(Constants.APPFCM_ID).then(fcmid => {
        this.http.callApiLogin("login", {
            "phnumber": this.loginForm.value.mobileno,
            "devicekey": fcmid
        }).then(async (data: any) => {
            this.msgHelper.dismiss();
            if (data.status === Constants.SUCCESS) {
                this.storage.setValue(Constants.TOKEN, data.token);
                const appUserId = data.data[0].DriverId;
                const appUserOTP = data.data[0].OTP;
                this.storage.setValue(Constants.USER_ID, data.data[0].DriverId);
                this.storage.setValue(Constants.MOBILE, this.loginForm.value.mobileno);
                console.log('appUserId ---', appUserId);
                console.log('appUserOTP ---', appUserOTP);
                // this.navCtrl.navigateForward('/registration');
                this.router.navigate(['/registration', {OTP: data.data[0].OTP}]);
                // const alert = await this.alertController.create({
                //     header: "OTP",
                //     message: "Please enter the OTP sent to the given mobile number.(Your OTP : )"+appUserOTP,
                //     inputs: [
                //         {
                //             name: 'otp',
                //             placeholder: 'OTP',
                //             type: 'number'
                //         },
                //     ],
                //     buttons: [
                //         {
                //             text: 'Cancel',
                //             role: 'cancel'
                //         },
                //         {
                //             text: 'OK',
                //             handler: data => {
                //                 if (data.otp) {
                //                     this.msgHelper.presentLoading();
                //                     this.http.callApi("drivervalidate", {
                //                         DriverId: appUserId,
                //                         OTP: data.otp
                //                     }).then((data: any) => {
                //                         this.msgHelper.dismiss();
                //                         if (data.status == Constants.HTTP_ERROR) {
                //                             this.msgHelper.presentAlertok("Wrong OTP", "The OTP you have entered is wrong. Please try again.");
                //                             return false;
                //                         } else if (data.status == Constants.SUCCESS) {
                //                             this.msgHelper.presentToast("Login successfully");
                //                             this.storage.setValue(Constants.USER_ID, data.data[0].DriverId);
                //                             this.storage.setValue(Constants.USER_NAME, data.data[0].Name);
                //                             this.storage.setValue(Constants.MOBILE, data.data[0].Phone);
                //                             this.storage.setValue(Constants.EMAIL, data.data[0].Email);
                //                             this.storage.setValue(Constants.ADDRESS, data.data[0].Address);
                //                             this.storage.setValue(Constants.PINCODE, data.data[0].Pincode);
                //                             this.storage.setValue(Constants.Landmark, data.data[0].Landmark);
                //                             this.storage.setValue(Constants.IMAGE, data.data[0].ProfileImage);
                //                             this.storage.setValue(Constants.USER_Online, data.data[0].IsOnline);
                //                             this.navCtrl.navigateForward('/menu/home');
                //                         } else {
                //                             this.msgHelper.showConnectionErrorDialog();
                //                             return false;
                //                         }
                //                     });
                //                 } else {
                //                     return false;
                //                 }
                //             }
                //         }
                //     ]
                // });
                //
                // alert.present();
                // let result = alert.onDidDismiss();
                // console.log(result);
            } else {
                this.msgHelper.presentToast("Mobile number not matched, Please check your mobile number.");
                //this.msgHelper.presentToast(data.message);
            }
        });
        });
    }

}
