import {Component, ViewChild, Directive, Renderer, ElementRef, OnInit} from '@angular/core';
import {
  NavController,
  LoadingController,
  Platform,
  ModalController,
  ActionSheetController,
  IonRouterOutlet
} from '@ionic/angular';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {MessageHelper} from 'src/providers/message-helper';
import {AppStorage} from 'src/providers/data/appstorage';
import {Constants} from 'src/utils/constants';
import {HttpProvider} from 'src/providers/data/data';
import { Geolocation } from '@ionic-native/geolocation/ngx';
declare var google;
@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  userID: any;
  allOrderlists: any = [];
  private start: number = 1;
  hasMoreData: boolean = true;
  lat: any;
  lon: any;
  mapOrder: any;
  passValues = false;
  constructor(public navCtrl: NavController, public msgHelper: MessageHelper, public loadingCtrl: LoadingController, public platform: Platform, public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController, public storage: AppStorage, public formBuilder: FormBuilder, private  router: Router, public modalController: ModalController, public element: ElementRef, public renderer: Renderer, public http: HttpProvider, public geolocation: Geolocation) {

    this.storage.getValue(Constants.USER_ID).then(userid => {
      this.userID = userid;
      console.log('orderPage userID', this.userID);
    });
  }

  ionViewWillEnter() {
    this.mapLoad();
    this.getAllOrders();
    this.start = 1;
  }
  mapLoad(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.lon = resp.coords.longitude;
      console.log('latitude --', this.lat);
      console.log('longitude --', this.lon);
      const uluru = {lat: this.lat, lng: this.lon};
      this.mapOrder = new google.maps.Map(document.getElementById('mapOrder'), {
        zoom: 13,
        center: uluru,
        disableDefaultUI: true,
      })
      var marker = new google.maps.Marker({
        position: uluru,
        map: this.mapOrder,
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
      });
    }).catch((error) => {
      console.log('Error getting location', error);
      // this.msgHelper.presentAlertok('Failed...', 'Do not fetch your location please check your gps connection.');
    });
  }

  ngOnInit() {
  }

  getAllOrders() {
    this.storage.getValue(Constants.USER_ID).then(userid => {
      this.http.callApi('completedorderlist', {
        'DriverId': userid,
        'OrderNo': '',
        'CustomerName': '',
        'CustomerPhone': '',
        'Page': '1',
        'Size': '10'
      }).then((data: any) => {
        if (data.status == Constants.SUCCESS) {
          if (!this.http.isNullOrEmpty(data)) {
            this.allOrderlists = data.data;
            console.log('allOrderlists 1st 10--', this.allOrderlists);
            this.passValues = true;
          }
        } else {
          this.msgHelper.presentAlertok('Failed', 'Please try again.');
        }
      });
    });
  }


  doInfinite(): Promise<any> {
    this.start += 1;
    // this.pageSize += 10;
    return new Promise((resolve) => {
      this.http.callApi('completedorderlist', {
        'DriverId': this.userID,
        'OrderNo': '',
        'CustomerName': '',
        'CustomerPhone': '',
        'Page': this.start,
        'Size': '10'
      }).then((data: any) => {
        //loader.dismiss();
        let count = 0;
        if (!this.http.isNullOrEmpty(data)) {
          console.log(data.data);
          this.allOrderlists = [].concat(this.allOrderlists, data.data);
          console.log('allOrderlists 2nd--', this.allOrderlists);

        }
        if (count > 0) {
          resolve();
        } else {
          this.hasMoreData = false;
        }
      }, (err) => {
        console.log(err);

        resolve();
      });
    });
  }
  doRefresh(refresher) {
    this.getAllOrders();
    console.log('Begin async operation', refresher);
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
}
