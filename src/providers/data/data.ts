import {Injectable} from '@angular/core';
import {AppStorage} from "./appstorage";
import {Constants} from "../../utils/constants";
import {HttpClient, HttpHeaders} from '@angular/common/http';
@Injectable()
export class HttpProvider {
    tokenId: any;

    constructor(private http: HttpClient, private storage: AppStorage) {
    }

    callApi(action, data) {
        return new Promise(resolve => {
            this.storage.getValue(Constants.TOKEN).then(count => {
                this.tokenId = count;
                console.log('Token Id --', this.tokenId);
                const headers = new HttpHeaders({
                    // 'Content-Type': 'application/json',
                    // 'Accept': 'application/json',
                    // 'Accept' : '*/*',
                    'Authorization': "Bearer" + " " + this.tokenId
                });
                const options = {
                    headers: headers
                };
                console.log(data);
                this.http.post('https://service.onlyalibaba.in/driverservice/' + action, data, options)
                // .map(res => res.json())
                    .subscribe(data => {
                            console.log(data);
                            resolve(data);
                        },
                        err => {
                            console.log(err);
                            resolve({status: Constants.HTTP_ERROR});
                        }
                    );
            });
        });
    }
    callApiLogin(action, data) {
        return new Promise(resolve => {
            const headers = new HttpHeaders({
                'Content-Type': 'application/json',
            });
            const options = {
                headers: headers
            };
            console.log(data);
            this.http.post('https://service.onlyalibaba.in/driverservice/' + action, data, options)
                .subscribe(data => {
                        console.log(data);
                        resolve(data);
                    },
                    err => {
                        console.log(err);
                        resolve({status: Constants.HTTP_ERROR});
                    }
                );
        });
    }
    isNullOrEmpty(str) {
        return typeof str === 'undefined' || str === null || (typeof str === 'string' && str.length <= 0);
    }

    convertToArray(object) {
        if (this.isArray(object))
            return object;
        else
            return [ object ];
    }

    isArray(object) {
        if (typeof object === 'undefined')
            return false;
        return Object.prototype.toString.call(object).slice(8, -1) === 'Array';
    }

}
