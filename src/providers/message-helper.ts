// import {Alert, AlertController, Loading, LoadingController, ToastController} from "ionic-angular";
import {LoadingController, AlertController} from '@ionic/angular';
import {Injectable} from "@angular/core";
import {ToastController} from '@ionic/angular';
@Injectable()
export class MessageHelper {

    constructor(public alertCtrl: AlertController, public  loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    }

    isLoading = false;

    async present() {
        this.isLoading = true;
        return await this.loadingCtrl.create({
            duration: 5000,
        }).then(a => {
            a.present().then(() => {
                console.log('presented');
                if (!this.isLoading) {
                    a.dismiss().then(() => console.log('abort presenting'));
                }
            });
        });
    }

    // presentLoadingDefault() {
    //   let loading = this.loadingCtrl.create({
    //     content: 'Please wait...'
    //   });

    //   loading.present();

    //   setTimeout(() => {
    //     loading.dismiss();
    //   }, 5000);
    // }
    async dismiss() {
        this.isLoading = false;
        return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
    }

    async presentToast(message: string, autoClose: boolean = true) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 2000
        });
        toast.present();
    }

    async presentLoading(message: string = "Please wait...") {
        const loading = await this.loadingCtrl.create({
            message: message,
            duration: 5000
        });
        await loading.present();

        // await loading.onDidDismiss();

        //  console.log('Loading dismissed!');
    }

    async presentAlert(title: string, msg: string, button: string = 'Close') {
        const alert = await this.alertCtrl.create({
            header: title,
            message: msg,
            buttons: [{
                text: "No"
            }, {
                text: "Yes",
                handler: () => {

                }
            }]
        });
        await alert.present();
    }

    async presentAlertok(title: string, msg: string, button: string = 'Close') {
        const alert = await this.alertCtrl.create({
            header: title,
            message: msg,
            buttons: [{
                text: "Ok"
            }]
        });
        await alert.present();
    }

    async showConnectionErrorDialog() {
        const alert = await this.alertCtrl.create({
            header: "Connection Error!",
            message: "Failed to connect with server. Please try again.",
            buttons: ['Close']
        });
        await alert.present();
    }

}
