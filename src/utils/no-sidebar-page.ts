import {MenuController} from "@ionic/angular";
/**
 * Created by Souvik Hazra on 07-05-2017.
 */

export class NoSidebarPage {

  protected constructor(public menu: MenuController) {
  }

  ionViewWillEnter() {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }
}
