export class ReturnCodes {
  static SYSTEM_ERROR = -1;
  static SUCCESS = 1;
  static FAILURE = 0;
  static INVALID_USER = -10;
  static PRODUCT_OUT_OF_STOCK = 2;
  static INSUFFICIENT_STOCK = 3;
  static CUT_OFF_TIME_EXCEEDED = 4;
  static INVALID_CITY = -11;

  static ORDER_ORDERED = 1;
  static ORDER_PAYMENT_CONFIRMED = 2;
  static ORDER_SHIPPED = 3;
  static ORDER_OUT_FOR_DELIVERY = 4;
  static ORDER_DELIVERED = 5;

  static ORDER_CANCELLED_BY_USER = 6;
  static ORDER_CANCELLED_BY_ADMIN = 7;
  static ORDER_RETURN = 8;
}
