export class Constants {
  public static SUCCESS: any = "200";
  public static NOT_FOUND: any = "404";
  public static WRONG: any = 401;
  public static HTTP_ERROR: any = "400";
  public static USER_ID: any = 'USER_ID';

  public static USER_Active: any = 'USER_active';
  public static USER_Online: any = 'USER_online';
  public static USER_NAME: string = 'user_name';
  public static APPFCM_ID: string = 'fcm_id';
  public static TOKEN: string = 'token';
  public static REF_ID: string = 'ref_id';
  public static MOBILE: any = 'ph_no';
  public static SEX: any = 'M';
  public static AGE: any = 18;
  public static ROLE: any = 0;
  public static HEIGHT: any = 'HEIGHT';
  public static LAB_ID: any = 'LAB_ID';
  public static WEIGHT: any = 'WEIGHT';
  public static DESEASE: any = 'DESEASE';
  public static ALERGY: any = 'ALERGY';
  public static HABITS: any = 'HABITS';
  public static AADHAR: any = 'SDSGDGSF5235632GDWVGEWT12332DFF3E34';
  public static APPDEVICE_KEY: any = "dssdffsedddxssaxxxssssssssss";
  public static EMAIL: string = 'email';
  public static PATIENT_ID: any = 0;
  public static ADDRESS: string = 'PATIENT_ID';
  public static PINCODE: string = 'pin_code';
  public static Landmark: string = 'landmark';
  public static PlaceLatitude : any = 'place_latitude';
  public static PlaceLongitude : any = 'Place_longitude';
  public static ATTENDANCEPOLICY : any = 'attendance_policy';
  public static LOGINFLAG : any = 0;
  public static EMPLOYEELIST : any = 'EMPLOYEELIST';
 
  public static IMAGE : any = 'IMAGE';
  public static STATUS : any = 'STATUS';
  public static PASSWORD : any = 'PASSWORD';
  // validator regex
  public static PHONE_REGEX = /^\d{10}$/;
  public static PIN_REGEX = /^\d{6}$/;
  public static EMAIL_REGEX = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  public static ADDID : any = 0;
  public static OPTED_IN = 'OptedIn';
  public static ID = 'id';
  public static NAME = 'name';
  public static OPTED_OUT = 'OptedOut';
  public static UNVERIFIED = 'UnVerified';
  public static BRANDID = 'brandid';
  public static BRANDNAME = 'BRANDNAME';
}
