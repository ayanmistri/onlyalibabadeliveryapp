(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\t<div class=\"wrap-login-v9 offset-top-obj\">\n\t\t<img src=\"assets/images/login.png\" class=\"top-img\">\n\t\t<div class=\"wrap-interval\">\n\t\t\t<div class=\"brand\">\n\t\t\t\t<img class=\"parallax-obj parallax-down-obj\" src=\"assets/images/logo.png\">\n\t\t\t</div>\n\t\t\t<div class=\"cate\">\n\t\t\t\t<img src=\"assets/images/cate.png\">\n\t\t\t</div>\n\t\t\t<div class=\"main-form\">\n\t\t\t\t<form novalidate class=\"ion-text-center\" [formGroup]=\"loginForm\">\n\t\t\t\t\t<h5>Delivery App</h5>\n\t\t\t\t\t<h6>Login With Your Valid Mobile Number</h6>\n\t\t\t\t\t<ion-item lines=\"none\">\n\t\t\t\t\t\t+91 <ion-input style=\"margin-left: 3px;\" type=\"tel\" maxlength=\"10\" formControlName=\"mobileno\" placeholder=\"**********\">\n\t\t\t\t\t\t</ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-button size=\"small\" color=\"primary\" [disabled]=\"!loginForm.valid\" (click)=\"signIn()\">Login</ion-button>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        exports: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@-webkit-keyframes init-ani {\n  0% {\n    opacity: 1;\n    -webkit-transform: translateY(-100vh) translateX(-10vw) translateY(-100vh);\n            transform: translateY(-100vh) translateX(-10vw) translateY(-100vh);\n  }\n  100% {\n    opacity: 0.5;\n    -webkit-transform: translateY(100vh) translateX(10vw) rotate(-45deg);\n            transform: translateY(100vh) translateX(10vw) rotate(-45deg);\n  }\n}\n@keyframes init-ani {\n  0% {\n    opacity: 1;\n    -webkit-transform: translateY(-100vh) translateX(-10vw) translateY(-100vh);\n            transform: translateY(-100vh) translateX(-10vw) translateY(-100vh);\n  }\n  100% {\n    opacity: 0.5;\n    -webkit-transform: translateY(100vh) translateX(10vw) rotate(-45deg);\n            transform: translateY(100vh) translateX(10vw) rotate(-45deg);\n  }\n}\n.wrap-login-v9 {\n  width: 100%;\n  height: 100%;\n}\n.wrap-login-v9 .brand img {\n  height: 75px;\n}\n.wrap-login-v9 .wrap-interval {\n  width: 100%;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-align: center;\n          align-items: center;\n  position: relative;\n  z-index: 99;\n  margin-top: -50px;\n  margin-bottom: 15px;\n}\n.wrap-login-v9 .wrap-interval .main-form {\n  position: relative;\n  z-index: 100;\n  width: 90%;\n  padding: 5%;\n  margin-top: 18%;\n  background: #f2f2f2;\n}\n.wrap-login-v9 .decor {\n  position: absolute;\n  z-index: 10;\n  height: 100%;\n  width: 100%;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n.wrap-login-v9 .decor .main-decor {\n  width: 150vw;\n  opacity: 0.1;\n}\n.wrap-login-v9 .decor .main-decor img {\n  width: 100%;\n}\n.wrap-login-v9 .decor .leaf1 {\n  position: absolute;\n  top: 30%;\n  height: 20px;\n  left: 15%;\n  width: 30px;\n  border-radius: 0px 50%;\n  -webkit-transform: rotate(95deg);\n          transform: rotate(95deg);\n  -webkit-animation-name: init-ani;\n          animation-name: init-ani;\n  -webkit-animation-duration: 4s;\n          animation-duration: 4s;\n  -webkit-animation-iteration-count: infinite;\n          animation-iteration-count: infinite;\n}\n.wrap-login-v9 .decor .leaf2 {\n  position: absolute;\n  top: 10%;\n  height: 30px;\n  left: 60%;\n  width: 35px;\n  border-radius: 0px 50%;\n  -webkit-transform: rotate(15deg);\n          transform: rotate(15deg);\n  -webkit-animation-name: init-ani;\n          animation-name: init-ani;\n  -webkit-animation-duration: 5s;\n          animation-duration: 5s;\n  -webkit-animation-iteration-count: infinite;\n          animation-iteration-count: infinite;\n}\n.wrap-login-v9 .decor .leaf3 {\n  position: absolute;\n  top: 70%;\n  height: 90px;\n  right: 10%;\n  width: 50px;\n  border-radius: 0px 50%;\n  -webkit-transform: rotate(75deg);\n          transform: rotate(75deg);\n  -webkit-animation-name: init-ani;\n          animation-name: init-ani;\n  -webkit-animation-duration: 3s;\n          animation-duration: 3s;\n  -webkit-animation-iteration-count: infinite;\n          animation-iteration-count: infinite;\n}\n.main-form ion-item {\n  margin-bottom: 10px;\n  color: #000;\n  font-size: 13px;\n  border-radius: 5px;\n  --min-height: 35px;\n}\n.create {\n  color: #666;\n  font-size: 14px;\n}\n.create span {\n  color: #e0c200;\n}\n.top-img {\n  position: relative;\n}\n.main-form ion-button {\n  --box-shadow: inherit;\n}\n.cate {\n  margin-top: 15px;\n}\n.main-form h6 {\n  margin-bottom: 15px;\n  font-size: 14px;\n  text-transform: uppercase;\n  margin-top: 0;\n}\n.main-form h5 {\n  top: -30px;\n  background: #f2f2f2;\n  display: inline-block;\n  position: relative;\n  margin: 0;\n  padding: 3px 8px;\n  font-size: 20px;\n  color: #666;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vRjpcXFVzZXJzXFxBeWFuXFxEcm9wYm94XFxuZXcgYWxpYmFiYSBkZWxpdmVyeSBhcHAvc3JjXFxhcHBcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJO0lBQUksVUFBQTtJQUFZLDBFQUFBO1lBQUEsa0VBQUE7RUNFbEI7RURERTtJQUFNLFlBQUE7SUFBYyxvRUFBQTtZQUFBLDREQUFBO0VDS3RCO0FBQ0Y7QURSQTtFQUNJO0lBQUksVUFBQTtJQUFZLDBFQUFBO1lBQUEsa0VBQUE7RUNFbEI7RURERTtJQUFNLFlBQUE7SUFBYyxvRUFBQTtZQUFBLDREQUFBO0VDS3RCO0FBQ0Y7QURIQTtFQUNDLFdBQUE7RUFDQSxZQUFBO0FDS0Q7QURIRTtFQUNDLFlBQUE7QUNLSDtBREZDO0VBQ0MsV0FBQTtFQUNBLG9CQUFBO0VBQUEsYUFBQTtFQUNBLDRCQUFBO0VBQUEsNkJBQUE7VUFBQSxzQkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDSUY7QURIRTtFQUNDLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDS0g7QURGQztFQUNDLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtBQ0lGO0FESEU7RUFDQyxZQUFBO0VBQ0csWUFBQTtBQ0tOO0FESkc7RUFDQyxXQUFBO0FDTUo7QURIRTtFQUNDLGtCQUFBO0VBQ0csUUFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7RUFDSCw4QkFBQTtVQUFBLHNCQUFBO0VBQ0EsMkNBQUE7VUFBQSxtQ0FBQTtBQ0tIO0FESEU7RUFDQyxrQkFBQTtFQUNHLFFBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLGdDQUFBO1VBQUEsd0JBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0gsOEJBQUE7VUFBQSxzQkFBQTtFQUNBLDJDQUFBO1VBQUEsbUNBQUE7QUNLSDtBREhFO0VBQ0Msa0JBQUE7RUFDRyxRQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtFQUNILDhCQUFBO1VBQUEsc0JBQUE7RUFDQSwyQ0FBQTtVQUFBLG1DQUFBO0FDS0g7QUREQTtFQUFxQixtQkFBQTtFQUFxQixXQUFBO0VBQWEsZUFBQTtFQUFpQixrQkFBQTtFQUFvQixrQkFBQTtBQ1M1RjtBRFJBO0VBQVMsV0FBQTtFQUFhLGVBQUE7QUNhdEI7QURaQTtFQUFjLGNBQUE7QUNnQmQ7QURmQTtFQUFVLGtCQUFBO0FDbUJWO0FEbEJBO0VBQXdCLHFCQUFBO0FDc0J4QjtBRHJCQTtFQUFPLGdCQUFBO0FDeUJQO0FEeEJBO0VBQWUsbUJBQUE7RUFBcUIsZUFBQTtFQUFpQix5QkFBQTtFQUEyQixhQUFBO0FDK0JoRjtBRDlCQTtFQUFlLFVBQUE7RUFBWSxtQkFBQTtFQUFxQixxQkFBQTtFQUF1QixrQkFBQTtFQUFvQixTQUFBO0VBQVcsZ0JBQUE7RUFBa0IsZUFBQTtFQUFpQixXQUFBO0FDeUN6SSIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5Aa2V5ZnJhbWVzIGluaXQtYW5pIHtcclxuICAgIDAlIHtvcGFjaXR5OiAxOyB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTEwMHZoKSB0cmFuc2xhdGVYKC0xMHZ3KSB0cmFuc2xhdGVZKC0xMDB2aCl9XHJcbiAgICAxMDAlIHtvcGFjaXR5OiAwLjU7IHRyYW5zZm9ybTogdHJhbnNsYXRlWSgxMDB2aCkgdHJhbnNsYXRlWCgxMHZ3KSByb3RhdGUoLTQ1ZGVnKX1cclxufVxyXG5cclxuLndyYXAtbG9naW4tdjl7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG5cdC5icmFuZHtcclxuXHRcdGltZ3tcclxuXHRcdFx0aGVpZ2h0OiA3NXB4O1xyXG5cdFx0fVxyXG5cdH1cclxuXHQud3JhcC1pbnRlcnZhbHtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0XHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdFx0ei1pbmRleDogOTk7XHJcblx0XHRtYXJnaW4tdG9wOiAtNTBweDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDE1cHg7XHJcblx0XHQubWFpbi1mb3Jte1xyXG5cdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdHotaW5kZXg6IDEwMDtcclxuXHRcdFx0d2lkdGg6IDkwJTtcclxuXHRcdFx0cGFkZGluZzogNSU7XHJcblx0XHRcdG1hcmdpbi10b3A6IDE4JTtcclxuXHRcdFx0YmFja2dyb3VuZDogI2YyZjJmMjtcclxuXHRcdH1cclxuXHR9XHJcblx0LmRlY29ye1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0ei1pbmRleDogMTA7XHJcblx0XHRoZWlnaHQ6IDEwMCU7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdHRvcDogMDtcclxuXHRcdGxlZnQ6IDA7XHJcblx0XHRyaWdodDogMDtcclxuXHRcdGJvdHRvbTogMDtcclxuXHRcdC5tYWluLWRlY29ye1xyXG5cdFx0XHR3aWR0aDogMTUwdnc7XHJcbiAgICBcdFx0b3BhY2l0eTogMC4xO1xyXG5cdFx0XHRpbWd7XHJcblx0XHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdC5sZWFmMXtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0ICAgIHRvcDogMzAlO1xyXG5cdFx0ICAgIGhlaWdodDogMjBweDtcclxuXHRcdCAgICBsZWZ0OiAxNSU7XHJcblx0XHQgICAgd2lkdGg6IDMwcHg7XHJcblx0XHQgICAgYm9yZGVyLXJhZGl1czogMHB4IDUwJTtcclxuXHRcdCAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5NWRlZyk7XHJcblx0XHQgICAgYW5pbWF0aW9uLW5hbWU6IGluaXQtYW5pO1xyXG5cdFx0XHRhbmltYXRpb24tZHVyYXRpb246IDRzO1xyXG5cdFx0XHRhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcclxuXHRcdH1cclxuXHRcdC5sZWFmMntcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0ICAgIHRvcDogMTAlO1xyXG5cdFx0ICAgIGhlaWdodDogMzBweDtcclxuXHRcdCAgICBsZWZ0OiA2MCU7XHJcblx0XHQgICAgd2lkdGg6IDM1cHg7XHJcblx0XHQgICAgYm9yZGVyLXJhZGl1czogMHB4IDUwJTtcclxuXHRcdCAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxNWRlZyk7XHJcblx0XHQgICAgYW5pbWF0aW9uLW5hbWU6IGluaXQtYW5pO1xyXG5cdFx0XHRhbmltYXRpb24tZHVyYXRpb246IDVzO1xyXG5cdFx0XHRhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcclxuXHRcdH1cclxuXHRcdC5sZWFmM3tcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0ICAgIHRvcDogNzAlO1xyXG5cdFx0ICAgIGhlaWdodDogOTBweDtcclxuXHRcdCAgICByaWdodDogMTAlO1xyXG5cdFx0ICAgIHdpZHRoOiA1MHB4O1xyXG5cdFx0ICAgIGJvcmRlci1yYWRpdXM6IDBweCA1MCU7XHJcblx0XHQgICAgdHJhbnNmb3JtOiByb3RhdGUoNzVkZWcpO1xyXG5cdFx0ICAgIGFuaW1hdGlvbi1uYW1lOiBpbml0LWFuaTtcclxuXHRcdFx0YW5pbWF0aW9uLWR1cmF0aW9uOiAzcztcclxuXHRcdFx0YW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogaW5maW5pdGU7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbi5tYWluLWZvcm0gaW9uLWl0ZW17IG1hcmdpbi1ib3R0b206IDEwcHg7IGNvbG9yOiAjMDAwOyBmb250LXNpemU6IDEzcHg7IGJvcmRlci1yYWRpdXM6IDVweDsgLS1taW4taGVpZ2h0OiAzNXB4OyB9XHJcbi5jcmVhdGV7IGNvbG9yOiAjNjY2OyBmb250LXNpemU6IDE0cHg7IH1cclxuLmNyZWF0ZSBzcGFueyBjb2xvcjogI2UwYzIwMDsgfVxyXG4udG9wLWltZ3sgcG9zaXRpb246IHJlbGF0aXZlOyB9XHJcbi5tYWluLWZvcm0gaW9uLWJ1dHRvbiB7IC0tYm94LXNoYWRvdzogaW5oZXJpdDsgfVxyXG4uY2F0ZXsgbWFyZ2luLXRvcDogMTVweDsgfVxyXG4ubWFpbi1mb3JtIGg2eyBtYXJnaW4tYm90dG9tOiAxNXB4OyBmb250LXNpemU6IDE0cHg7IHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7IG1hcmdpbi10b3A6IDA7IH1cclxuLm1haW4tZm9ybSBoNXsgdG9wOiAtMzBweDsgYmFja2dyb3VuZDogI2YyZjJmMjsgZGlzcGxheTogaW5saW5lLWJsb2NrOyBwb3NpdGlvbjogcmVsYXRpdmU7IG1hcmdpbjogMDsgcGFkZGluZzogM3B4IDhweDsgZm9udC1zaXplOiAyMHB4OyBjb2xvcjogIzY2NjsgfVxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuIiwiQGtleWZyYW1lcyBpbml0LWFuaSB7XG4gIDAlIHtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtMTAwdmgpIHRyYW5zbGF0ZVgoLTEwdncpIHRyYW5zbGF0ZVkoLTEwMHZoKTtcbiAgfVxuICAxMDAlIHtcbiAgICBvcGFjaXR5OiAwLjU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDEwMHZoKSB0cmFuc2xhdGVYKDEwdncpIHJvdGF0ZSgtNDVkZWcpO1xuICB9XG59XG4ud3JhcC1sb2dpbi12OSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4ud3JhcC1sb2dpbi12OSAuYnJhbmQgaW1nIHtcbiAgaGVpZ2h0OiA3NXB4O1xufVxuLndyYXAtbG9naW4tdjkgLndyYXAtaW50ZXJ2YWwge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiA5OTtcbiAgbWFyZ2luLXRvcDogLTUwcHg7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG4ud3JhcC1sb2dpbi12OSAud3JhcC1pbnRlcnZhbCAubWFpbi1mb3JtIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxMDA7XG4gIHdpZHRoOiA5MCU7XG4gIHBhZGRpbmc6IDUlO1xuICBtYXJnaW4tdG9wOiAxOCU7XG4gIGJhY2tncm91bmQ6ICNmMmYyZjI7XG59XG4ud3JhcC1sb2dpbi12OSAuZGVjb3Ige1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDEwO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBib3R0b206IDA7XG59XG4ud3JhcC1sb2dpbi12OSAuZGVjb3IgLm1haW4tZGVjb3Ige1xuICB3aWR0aDogMTUwdnc7XG4gIG9wYWNpdHk6IDAuMTtcbn1cbi53cmFwLWxvZ2luLXY5IC5kZWNvciAubWFpbi1kZWNvciBpbWcge1xuICB3aWR0aDogMTAwJTtcbn1cbi53cmFwLWxvZ2luLXY5IC5kZWNvciAubGVhZjEge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMzAlO1xuICBoZWlnaHQ6IDIwcHg7XG4gIGxlZnQ6IDE1JTtcbiAgd2lkdGg6IDMwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDBweCA1MCU7XG4gIHRyYW5zZm9ybTogcm90YXRlKDk1ZGVnKTtcbiAgYW5pbWF0aW9uLW5hbWU6IGluaXQtYW5pO1xuICBhbmltYXRpb24tZHVyYXRpb246IDRzO1xuICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcbn1cbi53cmFwLWxvZ2luLXY5IC5kZWNvciAubGVhZjIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTAlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIGxlZnQ6IDYwJTtcbiAgd2lkdGg6IDM1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDBweCA1MCU7XG4gIHRyYW5zZm9ybTogcm90YXRlKDE1ZGVnKTtcbiAgYW5pbWF0aW9uLW5hbWU6IGluaXQtYW5pO1xuICBhbmltYXRpb24tZHVyYXRpb246IDVzO1xuICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcbn1cbi53cmFwLWxvZ2luLXY5IC5kZWNvciAubGVhZjMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNzAlO1xuICBoZWlnaHQ6IDkwcHg7XG4gIHJpZ2h0OiAxMCU7XG4gIHdpZHRoOiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiAwcHggNTAlO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg3NWRlZyk7XG4gIGFuaW1hdGlvbi1uYW1lOiBpbml0LWFuaTtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiAzcztcbiAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogaW5maW5pdGU7XG59XG5cbi5tYWluLWZvcm0gaW9uLWl0ZW0ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIC0tbWluLWhlaWdodDogMzVweDtcbn1cblxuLmNyZWF0ZSB7XG4gIGNvbG9yOiAjNjY2O1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5jcmVhdGUgc3BhbiB7XG4gIGNvbG9yOiAjZTBjMjAwO1xufVxuXG4udG9wLWltZyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLm1haW4tZm9ybSBpb24tYnV0dG9uIHtcbiAgLS1ib3gtc2hhZG93OiBpbmhlcml0O1xufVxuXG4uY2F0ZSB7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5tYWluLWZvcm0gaDYge1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5tYWluLWZvcm0gaDUge1xuICB0b3A6IC0zMHB4O1xuICBiYWNrZ3JvdW5kOiAjZjJmMmYyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAzcHggOHB4O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiAjNjY2O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/message-helper */ "./src/providers/message-helper.ts");
/* harmony import */ var src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/data/appstorage */ "./src/providers/data/appstorage.ts");
/* harmony import */ var src_utils_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/utils/constants */ "./src/utils/constants.ts");
/* harmony import */ var src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/providers/data/data */ "./src/providers/data/data.ts");
/* harmony import */ var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/location-accuracy/ngx */ "./node_modules/@ionic-native/location-accuracy/ngx/index.js");











let LoginPage = class LoginPage {
    constructor(alertController, loadingController, router, formBuilder, msgHelper, storage, http, navCtrl, platform, locationAccuracy) {
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.router = router;
        this.formBuilder = formBuilder;
        this.msgHelper = msgHelper;
        this.storage = storage;
        this.http = http;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.locationAccuracy = locationAccuracy;
        // this.platform.ready().then(() => {
        //         this.fcm.getToken().then(token => {
        //             console.log('token' ,  token);
        //             console.log('registration ID app1---', token);
        //             this.appID = token;
        //             console.log('appID ID app2---', this.appID);
        //             this.storage.setValue(Constants.APPFCM_ID, this.appID);
        //         });
        //         this.fcm.onNotification().subscribe(data => {
        //             if(data.wasTapped){
        //                 console.log('Received in background');
        //             } else {
        //                 console.log('Received in foreground');
        //             }
        //         });
        //     });
        this.loginForm = formBuilder.group({
            mobileno: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(10)]]
        });
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].APPFCM_ID).then(fcmid => {
            this.appID = fcmid;
            console.log('LoginPage appID-', this.appID);
        });
        this.platform.backButton.subscribeWithPriority(0, () => {
            if (this.routerOutlet && this.routerOutlet.canGoBack()) {
                this.routerOutlet.pop();
            }
            else if (this.router.url === '/login') {
                navigator['app'].exitApp();
            }
            else {
                // this.msgHelper.presentToast("Exit", "Do you want to exit the app?", this.onYesHandler, this.onNoHandler, "backPress");
                // this.exitApp();
                this.router.navigate(['/login']);
                // this.msgHelper.presentToast('Please press again to exit app');
            }
        });
    }
    ngOnInit() {
    }
    signIn() {
        this.msgHelper.presentLoading();
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].APPFCM_ID).then(fcmid => {
            this.http.callApiLogin("login", {
                "phnumber": this.loginForm.value.mobileno,
                "devicekey": fcmid
            }).then((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.msgHelper.dismiss();
                if (data.status === src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                    this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].TOKEN, data.token);
                    const appUserId = data.data[0].DriverId;
                    const appUserOTP = data.data[0].OTP;
                    this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID, data.data[0].DriverId);
                    this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].MOBILE, this.loginForm.value.mobileno);
                    console.log('appUserId ---', appUserId);
                    console.log('appUserOTP ---', appUserOTP);
                    // this.navCtrl.navigateForward('/registration');
                    this.router.navigate(['/registration', { OTP: data.data[0].OTP }]);
                    // const alert = await this.alertController.create({
                    //     header: "OTP",
                    //     message: "Please enter the OTP sent to the given mobile number.(Your OTP : )"+appUserOTP,
                    //     inputs: [
                    //         {
                    //             name: 'otp',
                    //             placeholder: 'OTP',
                    //             type: 'number'
                    //         },
                    //     ],
                    //     buttons: [
                    //         {
                    //             text: 'Cancel',
                    //             role: 'cancel'
                    //         },
                    //         {
                    //             text: 'OK',
                    //             handler: data => {
                    //                 if (data.otp) {
                    //                     this.msgHelper.presentLoading();
                    //                     this.http.callApi("drivervalidate", {
                    //                         DriverId: appUserId,
                    //                         OTP: data.otp
                    //                     }).then((data: any) => {
                    //                         this.msgHelper.dismiss();
                    //                         if (data.status == Constants.HTTP_ERROR) {
                    //                             this.msgHelper.presentAlertok("Wrong OTP", "The OTP you have entered is wrong. Please try again.");
                    //                             return false;
                    //                         } else if (data.status == Constants.SUCCESS) {
                    //                             this.msgHelper.presentToast("Login successfully");
                    //                             this.storage.setValue(Constants.USER_ID, data.data[0].DriverId);
                    //                             this.storage.setValue(Constants.USER_NAME, data.data[0].Name);
                    //                             this.storage.setValue(Constants.MOBILE, data.data[0].Phone);
                    //                             this.storage.setValue(Constants.EMAIL, data.data[0].Email);
                    //                             this.storage.setValue(Constants.ADDRESS, data.data[0].Address);
                    //                             this.storage.setValue(Constants.PINCODE, data.data[0].Pincode);
                    //                             this.storage.setValue(Constants.Landmark, data.data[0].Landmark);
                    //                             this.storage.setValue(Constants.IMAGE, data.data[0].ProfileImage);
                    //                             this.storage.setValue(Constants.USER_Online, data.data[0].IsOnline);
                    //                             this.navCtrl.navigateForward('/menu/home');
                    //                         } else {
                    //                             this.msgHelper.showConnectionErrorDialog();
                    //                             return false;
                    //                         }
                    //                     });
                    //                 } else {
                    //                     return false;
                    //                 }
                    //             }
                    //         }
                    //     ]
                    // });
                    //
                    // alert.present();
                    // let result = alert.onDidDismiss();
                    // console.log(result);
                }
                else {
                    this.msgHelper.presentToast("Mobile number not matched, Please check your mobile number.");
                    //this.msgHelper.presentToast(data.message);
                }
            }));
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"] },
    { type: src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"] },
    { type: src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_9__["LocationAccuracy"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonRouterOutlet"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonRouterOutlet"])
], LoginPage.prototype, "routerOutlet", void 0);
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"], src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"], src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_9__["LocationAccuracy"]])
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=login-login-module-es2015.js.map