(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-contact-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppContactContactPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Past Order History\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <div class=\"mapOrder\" id=\"mapOrder\"></div>\n    <div class=\"spinner\" *ngIf=\"!passValues\">\n        <ion-spinner name=\"lines\"></ion-spinner>\n    </div>\n    <div *ngIf=\"passValues\">\n    <div style=\"margin-top: 0%;\" *ngFor=\"let order of allOrderlists\">\n\n      <ion-card class=\"ion-text-wrap order\">\n          <ion-card-header>\n              <ion-card-subtitle>Order Id: {{order.OrderNo}} <span> {{order.OrderDate}} | {{order.OrderTime}}</span></ion-card-subtitle>\n              <ion-card-title>{{order.Status}}</ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n              <ion-row class=\"address\">\n                  <ion-col size=\"2\">\n                      <ion-icon name=\"navigate\"></ion-icon>\n                  </ion-col>\n                  <ion-col size=\"10\">\n                      <p><b>{{order.AddressName}}</b><br>\n                          {{order.Address}}<br>\n                           {{'Pin- '+order.Pincode}}</p>\n                  </ion-col>\n              </ion-row>\n              <ion-row class=\"price\">\n                  <ion-col size=\"6\" text-left>\n                      Total: {{'₹'+order.OrderTotal}}<br>\n                      <small>Tip: ₹20</small>\n                  </ion-col>\n                  <ion-col size=\"6\" text-right>\n                      {{'Payment Mode: '+order.PaymentMode}}\n                  </ion-col>\n              </ion-row>\n          </ion-card-content>\n      </ion-card>\n      <ion-infinite-scroll threshold=\"100px\" id=\"infinite-scroll\" *ngIf=\"hasMoreData\" (ionInfinite)=\"$event.waitFor(doInfinite())\">\n          <ion-infinite-scroll-content style=\"min-height: 0px;\"\n                  loading-spinner=\"bubbles\"\n                  loading-text=\"Loading more data...\">\n          </ion-infinite-scroll-content>\n      </ion-infinite-scroll>\n  </div>\n        <div *ngIf=\"allOrderlists.length === 0\">\n            <img src=\"/assets/images/past.jpg\">\n        </div>\n    </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/contact/contact.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/contact/contact.module.ts ***!
    \*******************************************/

  /*! exports provided: ContactPageModule */

  /***/
  function srcAppContactContactModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactPageModule", function () {
      return ContactPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _contact_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./contact.page */
    "./src/app/contact/contact.page.ts");

    const routes = [{
      path: '',
      component: _contact_page__WEBPACK_IMPORTED_MODULE_6__["ContactPage"]
    }];
    let ContactPageModule = class ContactPageModule {};
    ContactPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_contact_page__WEBPACK_IMPORTED_MODULE_6__["ContactPage"]]
    })], ContactPageModule);
    /***/
  },

  /***/
  "./src/app/contact/contact.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/contact/contact.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppContactContactPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".wrap-section {\n  position: relative;\n}\n.wrap-section .text-section {\n  position: relative;\n}\n.mapOrder {\n  width: 100%;\n  height: 250px;\n  position: fixed;\n}\n.order ion-card-header {\n  background: #e0c200;\n  color: #000;\n  padding: 6px;\n}\n.order ion-card-subtitle span {\n  float: right;\n  font-size: 10px;\n}\n.order ion-card-subtitle {\n  color: #fff;\n  font-size: 12px;\n}\n.order ion-card-title {\n  font-size: 13px;\n  font-weight: 400;\n}\n.order {\n  background: #fafafa;\n}\n.order ion-card-content {\n  padding: 6px;\n}\n.address ion-icon {\n  font-size: 22px;\n  color: #fff;\n  background: #0f7b05;\n  border-radius: 50px;\n  padding: 4px;\n  margin-top: 28%;\n}\n.address p {\n  font-size: 12px;\n}\n.address p b {\n  font-size: 14px;\n  font-weight: 500;\n}\n.price ion-col {\n  font-size: 12px;\n  color: #ff304c;\n  font-weight: 700;\n}\n.price {\n  border-top: 1px solid #e8e8e8;\n}\n.spinner {\n  position: absolute;\n  top: 50%;\n  left: 45%;\n}\n.spinner ion-spinner {\n  width: 50px;\n  height: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC9GOlxcVXNlcnNcXEF5YW5cXERyb3Bib3hcXG5ldyBhbGliYWJhIGRlbGl2ZXJ5IGFwcC9zcmNcXGFwcFxcY29udGFjdFxcY29udGFjdC5wYWdlLnNjc3MiLCJzcmMvYXBwL2NvbnRhY3QvY29udGFjdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxrQkFBQTtBQ0NEO0FEQUM7RUFDQyxrQkFBQTtBQ0VGO0FEQ0E7RUFBVyxXQUFBO0VBQWEsYUFBQTtFQUFlLGVBQUE7QUNLdkM7QURKQTtFQUF3QixtQkFBQTtFQUFxQixXQUFBO0VBQWEsWUFBQTtBQ1UxRDtBRFRBO0VBQStCLFlBQUE7RUFBYyxlQUFBO0FDYzdDO0FEYkE7RUFBMEIsV0FBQTtFQUFhLGVBQUE7QUNrQnZDO0FEakJBO0VBQXVCLGVBQUE7RUFBaUIsZ0JBQUE7QUNzQnhDO0FEckJBO0VBQVEsbUJBQUE7QUN5QlI7QUR4QkE7RUFBeUIsWUFBQTtBQzRCekI7QUQzQkE7RUFBbUIsZUFBQTtFQUFpQixXQUFBO0VBQWEsbUJBQUE7RUFBcUIsbUJBQUE7RUFBcUIsWUFBQTtFQUFjLGVBQUE7QUNvQ3pHO0FEbkNBO0VBQVksZUFBQTtBQ3VDWjtBRHRDQTtFQUFjLGVBQUE7RUFBaUIsZ0JBQUE7QUMyQy9CO0FEMUNBO0VBQWdCLGVBQUE7RUFBaUIsY0FBQTtFQUFnQixnQkFBQTtBQ2dEakQ7QUQvQ0E7RUFBUSw2QkFBQTtBQ21EUjtBRGxEQTtFQUNDLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7QUNxREQ7QURuREE7RUFDQyxXQUFBO0VBQ0EsWUFBQTtBQ3NERCIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3QvY29udGFjdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud3JhcC1zZWN0aW9ue1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHQudGV4dC1zZWN0aW9ue1xyXG5cdFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdH1cclxufVxyXG4ubWFwT3JkZXJ7IHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDI1MHB4OyBwb3NpdGlvbjogZml4ZWQ7IH1cclxuLm9yZGVyIGlvbi1jYXJkLWhlYWRlcnsgYmFja2dyb3VuZDogI2UwYzIwMDsgY29sb3I6ICMwMDA7IHBhZGRpbmc6IDZweDsgfVxyXG4ub3JkZXIgaW9uLWNhcmQtc3VidGl0bGUgc3BhbnsgZmxvYXQ6IHJpZ2h0OyBmb250LXNpemU6IDEwcHg7IH1cclxuLm9yZGVyIGlvbi1jYXJkLXN1YnRpdGxleyBjb2xvcjogI2ZmZjsgZm9udC1zaXplOiAxMnB4OyB9XHJcbi5vcmRlciBpb24tY2FyZC10aXRsZXsgZm9udC1zaXplOiAxM3B4OyBmb250LXdlaWdodDogNDAwOyB9XHJcbi5vcmRlcnsgYmFja2dyb3VuZDogI2ZhZmFmYTsgfVxyXG4ub3JkZXIgaW9uLWNhcmQtY29udGVudHsgcGFkZGluZzogNnB4OyB9XHJcbi5hZGRyZXNzIGlvbi1pY29ueyBmb250LXNpemU6IDIycHg7IGNvbG9yOiAjZmZmOyBiYWNrZ3JvdW5kOiAjMGY3YjA1OyBib3JkZXItcmFkaXVzOiA1MHB4OyBwYWRkaW5nOiA0cHg7IG1hcmdpbi10b3A6IDI4JTsgfVxyXG4uYWRkcmVzcyBweyBmb250LXNpemU6IDEycHg7IH1cclxuLmFkZHJlc3MgcCBieyBmb250LXNpemU6IDE0cHg7IGZvbnQtd2VpZ2h0OiA1MDA7IH1cclxuLnByaWNlIGlvbi1jb2x7IGZvbnQtc2l6ZTogMTJweDsgY29sb3I6ICNmZjMwNGM7IGZvbnQtd2VpZ2h0OiA3MDA7IH1cclxuLnByaWNleyBib3JkZXItdG9wOiAxcHggc29saWQgI2U4ZThlODsgfVxyXG4uc3Bpbm5lcntcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0dG9wOiA1MCU7XHJcblx0bGVmdDogNDUlO1xyXG59XHJcbi5zcGlubmVyIGlvbi1zcGlubmVye1xyXG5cdHdpZHRoOiA1MHB4O1xyXG5cdGhlaWdodDogNTBweDtcclxufVxyXG5cclxuXHJcbiIsIi53cmFwLXNlY3Rpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ud3JhcC1zZWN0aW9uIC50ZXh0LXNlY3Rpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5tYXBPcmRlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDI1MHB4O1xuICBwb3NpdGlvbjogZml4ZWQ7XG59XG5cbi5vcmRlciBpb24tY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjZTBjMjAwO1xuICBjb2xvcjogIzAwMDtcbiAgcGFkZGluZzogNnB4O1xufVxuXG4ub3JkZXIgaW9uLWNhcmQtc3VidGl0bGUgc3BhbiB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxMHB4O1xufVxuXG4ub3JkZXIgaW9uLWNhcmQtc3VidGl0bGUge1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ub3JkZXIgaW9uLWNhcmQtdGl0bGUge1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG5cbi5vcmRlciB7XG4gIGJhY2tncm91bmQ6ICNmYWZhZmE7XG59XG5cbi5vcmRlciBpb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogNnB4O1xufVxuXG4uYWRkcmVzcyBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6ICMwZjdiMDU7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIHBhZGRpbmc6IDRweDtcbiAgbWFyZ2luLXRvcDogMjglO1xufVxuXG4uYWRkcmVzcyBwIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uYWRkcmVzcyBwIGIge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5wcmljZSBpb24tY29sIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogI2ZmMzA0YztcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLnByaWNlIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlOGU4ZTg7XG59XG5cbi5zcGlubmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNDUlO1xufVxuXG4uc3Bpbm5lciBpb24tc3Bpbm5lciB7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/contact/contact.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/contact/contact.page.ts ***!
    \*****************************************/

  /*! exports provided: ContactPage */

  /***/
  function srcAppContactContactPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactPage", function () {
      return ContactPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/message-helper */
    "./src/providers/message-helper.ts");
    /* harmony import */


    var src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/data/appstorage */
    "./src/providers/data/appstorage.ts");
    /* harmony import */


    var src_utils_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/utils/constants */
    "./src/utils/constants.ts");
    /* harmony import */


    var src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/providers/data/data */
    "./src/providers/data/data.ts");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");

    let ContactPage = class ContactPage {
      constructor(navCtrl, msgHelper, loadingCtrl, platform, modalCtrl, actionSheetCtrl, storage, formBuilder, router, modalController, element, renderer, http, geolocation) {
        this.navCtrl = navCtrl;
        this.msgHelper = msgHelper;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.router = router;
        this.modalController = modalController;
        this.element = element;
        this.renderer = renderer;
        this.http = http;
        this.geolocation = geolocation;
        this.allOrderlists = [];
        this.start = 1;
        this.hasMoreData = true;
        this.passValues = false;
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID).then(userid => {
          this.userID = userid;
          console.log('orderPage userID', this.userID);
        });
      }

      ionViewWillEnter() {
        this.mapLoad();
        this.getAllOrders();
        this.start = 1;
      }

      mapLoad() {
        this.geolocation.getCurrentPosition().then(resp => {
          this.lat = resp.coords.latitude;
          this.lon = resp.coords.longitude;
          console.log('latitude --', this.lat);
          console.log('longitude --', this.lon);
          const uluru = {
            lat: this.lat,
            lng: this.lon
          };
          this.mapOrder = new google.maps.Map(document.getElementById('mapOrder'), {
            zoom: 13,
            center: uluru,
            disableDefaultUI: true
          });
          var marker = new google.maps.Marker({
            position: uluru,
            map: this.mapOrder,
            icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
          });
        }).catch(error => {
          console.log('Error getting location', error); // this.msgHelper.presentAlertok('Failed...', 'Do not fetch your location please check your gps connection.');
        });
      }

      ngOnInit() {}

      getAllOrders() {
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID).then(userid => {
          this.http.callApi('completedorderlist', {
            'DriverId': userid,
            'OrderNo': '',
            'CustomerName': '',
            'CustomerPhone': '',
            'Page': '1',
            'Size': '10'
          }).then(data => {
            if (data.status == src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
              if (!this.http.isNullOrEmpty(data)) {
                this.allOrderlists = data.data;
                console.log('allOrderlists 1st 10--', this.allOrderlists);
                this.passValues = true;
              }
            } else {
              this.msgHelper.presentAlertok('Failed', 'Please try again.');
            }
          });
        });
      }

      doInfinite() {
        this.start += 1; // this.pageSize += 10;

        return new Promise(resolve => {
          this.http.callApi('completedorderlist', {
            'DriverId': this.userID,
            'OrderNo': '',
            'CustomerName': '',
            'CustomerPhone': '',
            'Page': this.start,
            'Size': '10'
          }).then(data => {
            //loader.dismiss();
            let count = 0;

            if (!this.http.isNullOrEmpty(data)) {
              console.log(data.data);
              this.allOrderlists = [].concat(this.allOrderlists, data.data);
              console.log('allOrderlists 2nd--', this.allOrderlists);
            }

            if (count > 0) {
              resolve();
            } else {
              this.hasMoreData = false;
            }
          }, err => {
            console.log(err);
            resolve();
          });
        });
      }

      doRefresh(refresher) {
        this.getAllOrders();
        console.log('Begin async operation', refresher);
        setTimeout(() => {
          console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
      }

    };

    ContactPage.ctorParameters = () => [{
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
    }, {
      type: src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]
    }, {
      type: src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"]
    }, {
      type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"]
    }, {
      type: src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"]
    }, {
      type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"]
    }];

    ContactPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-contact',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./contact.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./contact.page.scss */
      "./src/app/contact/contact.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"], src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"]])], ContactPage);
    /***/
  }
}]);
//# sourceMappingURL=contact-contact-module-es5.js.map