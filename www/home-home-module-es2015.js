(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n    <ion-toolbar color=\"primary\">\n        <ion-buttons slot=\"start\">\n            <ion-menu-button></ion-menu-button>\n        </ion-buttons>\n        <ion-title>\n            Dashboard\n        </ion-title>\n        <ion-label style=\"font-size: 10px; font-weight: 500;\" *ngIf=\"toggleStatus == 'true'\">On Duty</ion-label>\n        <ion-label style=\"font-size: 10px; font-weight: 500;\" *ngIf=\"toggleStatus == 'false'\">Off Duty</ion-label>\n        <ion-toggle slot=\"end\" color=\"dark\" *ngIf=\"toggleStatus == 'true'\" [checked]=\"true\" (ionChange)=\"myChange()\"></ion-toggle>\n        <ion-toggle slot=\"end\" color=\"dark\" *ngIf=\"toggleStatus == 'false'\" [checked]=\"false\" (ionChange)=\"myChange()\"></ion-toggle>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <div class=\"map\" id=\"map\"></div>\n    <ion-refresher (ionRefresh)=\"doRefresh($event);\">\n        <ion-refresher-content\n                pulling-icon=\"chevron-down-circle-outline\"\n                pulling-text=\"Pull to refresh\"\n                refreshing-spinner=\"circles\"\n                refreshing-text=\"Refreshing...\">\n        </ion-refresher-content>\n    </ion-refresher>\n    <div class=\"spinner\" *ngIf=\"!passValues\">\n        <ion-spinner name=\"lines\"></ion-spinner>\n    </div>\n    <div *ngIf=\"passValues\">\n    <div style=\"margin-top: 60%;\">\n        <ion-card class=\"ion-text-wrap order\" *ngFor=\"let order of allOrderlists\" (click)=\"orderDetails(order)\">\n            <ion-card-header>\n                <ion-card-subtitle>Order Id: {{order.OrderNo}} <span>{{order.OrderDate}} | {{order.OrderTime}}</span></ion-card-subtitle>\n                <ion-card-title>{{order.OrderDetail}}</ion-card-title>\n            </ion-card-header>\n            <ion-card-content>\n                <ion-row class=\"address\">\n                    <ion-col size=\"2\">\n                        <ion-icon name=\"navigate\"></ion-icon>\n                    </ion-col>\n                    <ion-col size=\"10\">\n                        <p><b>{{order.AddressName}}</b><br>\n                            {{order.Address}}<br>\n                            {{'Pin- '+order.Pincode}}</p>\n                    </ion-col>\n                </ion-row>\n                <ion-row class=\"price\">\n                    <ion-col size=\"6\" text-left>\n                        Total: {{'₹'+order.OrderTotal}}\n                    </ion-col>\n                    <ion-col size=\"6\" text-right>\n                        Tip: ₹20\n                    </ion-col>\n                </ion-row>\n            </ion-card-content>\n        </ion-card>\n        <ion-infinite-scroll threshold=\"100px\" id=\"infinite-scroll\" *ngIf=\"hasMoreData\" (ionInfinite)=\"$event.waitFor(doInfinite())\">\n            <ion-infinite-scroll-content style=\"min-height: 0px;\"\n                    loading-spinner=\"bubbles\"\n                    loading-text=\"Loading more data...\">\n            </ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n    </div>\n        <div *ngIf=\"allOrderlists.length === 0\">\n            <div text-center>\n                <ion-icon name=\"information-circle-outline\" style=\"font-size: 50px;padding-top: 200px;color: #E66A25;\"></ion-icon>\n            </div>\n            <div text-center style=\"font-size: 17px;color: black;\">No Data Available</div>\n        </div>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: '',
                    component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                }
            ])
        ],
        exports: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrap-section {\n  position: relative;\n}\n.wrap-section .text-section {\n  position: relative;\n}\n.map {\n  width: 100%;\n  height: 250px;\n  position: fixed !important;\n  top: 50px;\n}\n.order ion-card-header {\n  background: #01bb0d;\n  color: #000;\n  padding: 6px;\n}\n.order ion-card-subtitle span {\n  float: right;\n  font-size: 10px;\n}\n.order ion-card-subtitle {\n  color: #fff;\n  font-size: 12px;\n}\n.order ion-card-title {\n  font-size: 13px;\n  font-weight: 600;\n  color: #fff;\n}\n.order {\n  background: #fafafa;\n}\n.order ion-card-content {\n  padding: 6px;\n}\n.address ion-icon {\n  font-size: 22px;\n  color: #fff;\n  background: #0f7b05;\n  border-radius: 50px;\n  padding: 4px;\n  margin-top: 28%;\n}\n.address p {\n  font-size: 12px;\n}\n.address p b {\n  font-size: 14px;\n  font-weight: 500;\n}\n.price ion-col {\n  font-size: 12px;\n  color: #ff304c;\n  font-weight: 700;\n}\n.price {\n  border-top: 1px solid #e8e8e8;\n}\n.spinner {\n  position: absolute;\n  top: 50%;\n  left: 45%;\n}\n.spinner ion-spinner {\n  width: 50px;\n  height: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9GOlxcVXNlcnNcXEF5YW5cXERyb3Bib3hcXG5ldyBhbGliYWJhIGRlbGl2ZXJ5IGFwcC9zcmNcXGFwcFxcaG9tZVxcaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxrQkFBQTtBQ0NEO0FEQUM7RUFDQyxrQkFBQTtBQ0VGO0FEQ0E7RUFBTSxXQUFBO0VBQWEsYUFBQTtFQUFlLDBCQUFBO0VBQTRCLFNBQUE7QUNNOUQ7QURMQTtFQUF3QixtQkFBQTtFQUFxQixXQUFBO0VBQWEsWUFBQTtBQ1cxRDtBRFZBO0VBQStCLFlBQUE7RUFBYyxlQUFBO0FDZTdDO0FEZEE7RUFBMEIsV0FBQTtFQUFhLGVBQUE7QUNtQnZDO0FEbEJBO0VBQXVCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0IsV0FBQTtBQ3dCMUQ7QUR2QkE7RUFBUSxtQkFBQTtBQzJCUjtBRDFCQTtFQUF5QixZQUFBO0FDOEJ6QjtBRDdCQTtFQUFtQixlQUFBO0VBQWlCLFdBQUE7RUFBYSxtQkFBQTtFQUFxQixtQkFBQTtFQUFxQixZQUFBO0VBQWMsZUFBQTtBQ3NDekc7QURyQ0E7RUFBWSxlQUFBO0FDeUNaO0FEeENBO0VBQWMsZUFBQTtFQUFpQixnQkFBQTtBQzZDL0I7QUQ1Q0E7RUFBZ0IsZUFBQTtFQUFpQixjQUFBO0VBQWdCLGdCQUFBO0FDa0RqRDtBRGpEQTtFQUFRLDZCQUFBO0FDcURSO0FEcERBO0VBQ0Msa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtBQ3VERDtBRHJEQTtFQUNDLFdBQUE7RUFDQSxZQUFBO0FDd0REIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53cmFwLXNlY3Rpb257XG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcblx0LnRleHQtc2VjdGlvbntcblx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XG5cdH1cbn1cbi5tYXB7IHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDI1MHB4OyBwb3NpdGlvbjogZml4ZWQgIWltcG9ydGFudDsgdG9wOiA1MHB4OyB9XG4ub3JkZXIgaW9uLWNhcmQtaGVhZGVyeyBiYWNrZ3JvdW5kOiAjMDFiYjBkOyBjb2xvcjogIzAwMDsgcGFkZGluZzogNnB4OyB9XG4ub3JkZXIgaW9uLWNhcmQtc3VidGl0bGUgc3BhbnsgZmxvYXQ6IHJpZ2h0OyBmb250LXNpemU6IDEwcHg7IH1cbi5vcmRlciBpb24tY2FyZC1zdWJ0aXRsZXsgY29sb3I6ICNmZmY7IGZvbnQtc2l6ZTogMTJweDsgfVxuLm9yZGVyIGlvbi1jYXJkLXRpdGxleyBmb250LXNpemU6IDEzcHg7IGZvbnQtd2VpZ2h0OiA2MDA7IGNvbG9yOiAjZmZmOyB9XG4ub3JkZXJ7IGJhY2tncm91bmQ6ICNmYWZhZmE7IH1cbi5vcmRlciBpb24tY2FyZC1jb250ZW50eyBwYWRkaW5nOiA2cHg7IH1cbi5hZGRyZXNzIGlvbi1pY29ueyBmb250LXNpemU6IDIycHg7IGNvbG9yOiAjZmZmOyBiYWNrZ3JvdW5kOiAjMGY3YjA1OyBib3JkZXItcmFkaXVzOiA1MHB4OyBwYWRkaW5nOiA0cHg7IG1hcmdpbi10b3A6IDI4JTsgfVxuLmFkZHJlc3MgcHsgZm9udC1zaXplOiAxMnB4OyB9XG4uYWRkcmVzcyBwIGJ7IGZvbnQtc2l6ZTogMTRweDsgZm9udC13ZWlnaHQ6IDUwMDsgfVxuLnByaWNlIGlvbi1jb2x7IGZvbnQtc2l6ZTogMTJweDsgY29sb3I6ICNmZjMwNGM7IGZvbnQtd2VpZ2h0OiA3MDA7IH1cbi5wcmljZXsgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlOGU4ZTg7IH1cbi5zcGlubmVye1xuXHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdHRvcDogNTAlO1xuXHRsZWZ0OiA0NSU7XG59XG4uc3Bpbm5lciBpb24tc3Bpbm5lcntcblx0d2lkdGg6IDUwcHg7XG5cdGhlaWdodDogNTBweDtcbn1cblxuXG5cbiIsIi53cmFwLXNlY3Rpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ud3JhcC1zZWN0aW9uIC50ZXh0LXNlY3Rpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5tYXAge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyNTBweDtcbiAgcG9zaXRpb246IGZpeGVkICFpbXBvcnRhbnQ7XG4gIHRvcDogNTBweDtcbn1cblxuLm9yZGVyIGlvbi1jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQ6ICMwMWJiMGQ7XG4gIGNvbG9yOiAjMDAwO1xuICBwYWRkaW5nOiA2cHg7XG59XG5cbi5vcmRlciBpb24tY2FyZC1zdWJ0aXRsZSBzcGFuIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEwcHg7XG59XG5cbi5vcmRlciBpb24tY2FyZC1zdWJ0aXRsZSB7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5vcmRlciBpb24tY2FyZC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5vcmRlciB7XG4gIGJhY2tncm91bmQ6ICNmYWZhZmE7XG59XG5cbi5vcmRlciBpb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogNnB4O1xufVxuXG4uYWRkcmVzcyBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6ICMwZjdiMDU7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIHBhZGRpbmc6IDRweDtcbiAgbWFyZ2luLXRvcDogMjglO1xufVxuXG4uYWRkcmVzcyBwIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uYWRkcmVzcyBwIGIge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5wcmljZSBpb24tY29sIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogI2ZmMzA0YztcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLnByaWNlIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlOGU4ZTg7XG59XG5cbi5zcGlubmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNDUlO1xufVxuXG4uc3Bpbm5lciBpb24tc3Bpbm5lciB7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/message-helper */ "./src/providers/message-helper.ts");
/* harmony import */ var src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/data/appstorage */ "./src/providers/data/appstorage.ts");
/* harmony import */ var src_utils_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/utils/constants */ "./src/utils/constants.ts");
/* harmony import */ var src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/providers/data/data */ "./src/providers/data/data.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/location-accuracy/ngx */ "./node_modules/@ionic-native/location-accuracy/ngx/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");












let HomePage = class HomePage {
    constructor(navCtrl, msgHelper, loadingCtrl, platform, modalCtrl, actionSheetCtrl, storage, formBuilder, router, modalController, element, renderer, http, geolocation, locationAccuracy, androidPermissions) {
        this.navCtrl = navCtrl;
        this.msgHelper = msgHelper;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.router = router;
        this.modalController = modalController;
        this.element = element;
        this.renderer = renderer;
        this.http = http;
        this.geolocation = geolocation;
        this.locationAccuracy = locationAccuracy;
        this.androidPermissions = androidPermissions;
        this.allOrderlists = [];
        this.start = 1;
        this.pageSize = 10;
        this.hasMoreData = true;
        this.passValues = false;
        this.isToggled = true;
        this.platform.ready().then(() => {
            this.geolocation.getCurrentPosition().then((resp) => {
                this.lat = resp.coords.latitude;
                this.lon = resp.coords.longitude;
                console.log('latitude --', this.lat);
                console.log('longitude --', this.lon);
                const uluru = { lat: this.lat, lng: this.lon };
                this.map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 13,
                    center: uluru,
                    disableDefaultUI: true,
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: this.map,
                    icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
                });
            }).catch((error) => {
                console.log('Error getting location', error);
                // this.msgHelper.presentAlertok('Failed...', 'Do not fetch your location please check your gps connection.');
            });
        });
        this.platform.backButton.subscribeWithPriority(0, () => {
            if (this.routerOutlet && this.routerOutlet.canGoBack()) {
                this.routerOutlet.pop();
            }
            else if (this.router.url === '/login') {
                navigator['app'].exitApp();
            }
            else if (this.router.url === '/menu/home') {
                navigator['app'].exitApp();
            }
            else {
                // this.msgHelper.presentToast("Exit", "Do you want to exit the app?", this.onYesHandler, this.onNoHandler, "backPress");
                // this.exitApp();
                this.router.navigate(['/menu/home']);
                this.msgHelper.presentToast('Please press again to exit app');
            }
        });
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID).then(userid => {
            this.userID = userid;
            console.log('HomePage userID', this.userID);
        });
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_NAME).then(name => {
            this.name = name;
            console.log('HomePage userName', this.name);
        });
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].MOBILE).then(phno => {
            this.mobile = phno;
            console.log('HomePage mobile', this.mobile);
        });
        this.getUserData();
    }
    //Check if application having GPS access permission
    checkGPSPermission() {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(result => {
            if (result.hasPermission) {
                //If having permission show 'Turn On GPS' dialogue
                this.askToTurnOnGPS();
            }
            else {
                //If not having permission ask for permission
                this.requestGPSPermission();
            }
        }, err => {
            // alert(err);
        });
    }
    requestGPSPermission() {
        this.locationAccuracy.canRequest().then((canRequest) => {
            if (canRequest) {
                console.log("4");
            }
            else {
                //Show 'GPS Permission Request' dialogue
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(() => {
                    // call method to turn on GPS
                    this.askToTurnOnGPS();
                }, error => {
                    //Show alert if user click on 'No Thanks'
                    // alert('requestPermission Error requesting location permissions ' + error);
                });
            }
        });
    }
    askToTurnOnGPS() {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
            // When GPS Turned ON call method to get Accurate location coordinates
            this.getLocationCoordinates();
        });
    }
    // Methos to get device accurate coordinates using device GPS
    getLocationCoordinates() {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.lat = resp.coords.latitude;
            this.lon = resp.coords.longitude;
            this.mapLoad();
        }).catch((error) => {
            console.log('Error getting location', error);
        });
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.checkGPSPermission();
        // this.mapLoad();
        // this.getUserData();
        this.getAllOrders();
        this.start = 1;
    }
    mapLoad() {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.lat = resp.coords.latitude;
            this.lon = resp.coords.longitude;
            console.log('latitude --', this.lat);
            console.log('longitude --', this.lon);
            const uluru = { lat: this.lat, lng: this.lon };
            this.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: uluru,
                disableDefaultUI: true,
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: this.map,
                icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
            });
        }).catch((error) => {
            console.log('Error getting location', error);
            // this.msgHelper.presentAlertok('Failed...', 'Do not fetch your location please check your gps connection.');
        });
    }
    getUserData() {
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID).then(userid => {
            this.platform.ready().then(() => {
                this.http.callApi('driverdetails', {
                    'DriverId': userid,
                }).then((data) => {
                    if (data.status == src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                        this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_NAME, data.data[0].Name);
                        this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].MOBILE, data.data[0].Phone);
                        this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].EMAIL, data.data[0].Email);
                        this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].ADDRESS, data.data[0].Address);
                        this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].PINCODE, data.data[0].Pincode);
                        this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].Landmark, data.data[0].Landmark);
                        this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].IMAGE, data.data[0].ProfileImage);
                        this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_Online, data.data[0].IsOnline);
                        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_Online).then(togglestatus => {
                            this.toggleStatus = togglestatus;
                            console.log('HomePage toggleStatus---', this.toggleStatus);
                        });
                    }
                    else {
                        this.msgHelper.presentAlertok('Failed', 'Please try again.');
                    }
                });
            });
        });
    }
    getAllOrders() {
        // this.msgHelper.presentLoading();
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID).then(userid => {
            this.http.callApi('pendingorderlist', {
                'DriverId': userid,
                'OrderNo': '',
                'CustomerName': '',
                'CustomerPhone': '',
                'Page': '1',
                'Size': '10'
            }).then((data) => {
                // this.msgHelper.dismiss();
                if (data.status == src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                    if (!this.http.isNullOrEmpty(data)) {
                        this.allOrderlists = data.data;
                        console.log('allOrderlists 1st 10--', this.allOrderlists);
                        this.passValues = true;
                    }
                }
                else {
                    this.msgHelper.presentAlertok('Failed', 'Please try again.');
                }
            });
        });
    }
    doInfinite() {
        this.start += 1;
        // this.pageSize += 10;
        return new Promise((resolve) => {
            this.http.callApi('pendingorderlist', {
                'DriverId': this.userID,
                'OrderNo': '',
                'CustomerName': '',
                'CustomerPhone': '',
                'Page': this.start,
                'Size': '10'
            }).then((data) => {
                let count = 0;
                if (!this.http.isNullOrEmpty(data)) {
                    console.log(data.data);
                    this.allOrderlists = [].concat(this.allOrderlists, data.data);
                    console.log('allOrderlists 2nd--', this.allOrderlists);
                }
                if (count > 0) {
                    resolve();
                }
                else {
                    this.hasMoreData = false;
                }
            }, (err) => {
                console.log(err);
                resolve();
            });
        });
    }
    myChange() {
        if (this.toggleStatus == 'true') {
            this.msgHelper.presentLoading();
            this.http.callApi('driverstatusupdate', {
                'IsOnline': 'false',
                'DriverId': this.userID
            }).then((data) => {
                this.msgHelper.dismiss();
                if (data.status == src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                    this.getUserData();
                    // this.dutyStatus == "On Duty";
                    this.msgHelper.presentToast('You are offline...');
                    this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_Online, 'false');
                }
                else {
                    this.msgHelper.presentAlertok('Failed', 'Please try again.');
                }
            });
        }
        else if (this.toggleStatus == 'false') {
            this.msgHelper.presentLoading();
            this.http.callApi('driverstatusupdate', {
                'IsOnline': 'true',
                'DriverId': this.userID
            }).then((data) => {
                this.msgHelper.dismiss();
                if (data.status == src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                    this.getUserData();
                    // this.dutyStatus == "On Duty";
                    this.msgHelper.presentToast('You are online...');
                    this.storage.setValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_Online, 'true');
                }
                else {
                    this.msgHelper.presentAlertok('Failed', 'Please try again.');
                }
            });
        }
    }
    orderDetails(order) {
        this.router.navigate(['/product-details', { orderNo: order.OrderNo }]);
    }
    doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        this.getAllOrders();
        this.getUserData();
        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"] },
    { type: src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"] },
    { type: _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_10__["LocationAccuracy"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__["AndroidPermissions"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonRouterOutlet"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonRouterOutlet"])
], HomePage.prototype, "routerOutlet", void 0);
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"], src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"], _ionic_native_location_accuracy_ngx__WEBPACK_IMPORTED_MODULE_10__["LocationAccuracy"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__["AndroidPermissions"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map