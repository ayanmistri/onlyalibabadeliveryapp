(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["about-about-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Order\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content common-parallax [scrollEvents]=\"true\">\n  <div class=\"spinner\" *ngIf=\"!passValues\">\n    <ion-spinner name=\"lines\"></ion-spinner>\n  </div>\n  <div *ngIf=\"passValues\">\n  <div style=\"margin-top: 0%;\" *ngFor=\"let order of allOrderlists\">\n    <ion-card class=\"ion-text-wrap order\" (click)=\"orderDetails(order)\">\n      <ion-card-header>\n        <ion-card-subtitle>Order Id: {{order.OrderNo}} <span>{{order.OrderDate}} | {{order.OrderTime}}</span></ion-card-subtitle>\n        <ion-card-title>{{order.OrderDetail}}</ion-card-title>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-row class=\"address\">\n          <ion-col size=\"2\">\n            <ion-icon name=\"navigate\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <p><b>{{order.AddressName}}</b><br>\n              {{order.Address}}<br>\n              {{'Pin- '+order.Pincode}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row class=\"price\">\n          <ion-col size=\"6\" text-left>\n            Total: {{'₹'+order.OrderTotal}}\n          </ion-col>\n          <ion-col size=\"6\" text-right>\n            Tip: ₹20\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n    <ion-infinite-scroll threshold=\"100px\" id=\"infinite-scroll\" *ngIf=\"hasMoreData\" (ionInfinite)=\"$event.waitFor(doInfinite())\">\n      <ion-infinite-scroll-content style=\"min-height: 0px;\"\n              loading-spinner=\"bubbles\"\n              loading-text=\"Loading more data...\">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </div>\n    <div *ngIf=\"allOrderlists.length === 0\">\n      <div text-center>\n        <ion-icon name=\"information-circle-outline\" style=\"font-size: 50px;padding-top: 200px;color: #E66A25;\"></ion-icon>\n      </div>\n      <div text-center style=\"font-size: 17px;color: black;\">No Data Available</div>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/about/about.module.ts":
/*!***************************************!*\
  !*** ./src/app/about/about.module.ts ***!
  \***************************************/
/*! exports provided: AboutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPageModule", function() { return AboutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _about_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./about.page */ "./src/app/about/about.page.ts");







const routes = [
    {
        path: '',
        component: _about_page__WEBPACK_IMPORTED_MODULE_6__["AboutPage"]
    }
];
let AboutPageModule = class AboutPageModule {
};
AboutPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_about_page__WEBPACK_IMPORTED_MODULE_6__["AboutPage"]]
    })
], AboutPageModule);



/***/ }),

/***/ "./src/app/about/about.page.scss":
/*!***************************************!*\
  !*** ./src/app/about/about.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrap-section {\n  position: relative;\n}\n.wrap-section .text-section {\n  position: relative;\n}\n.map {\n  width: 100%;\n  height: 250px;\n  position: fixed;\n}\n.order ion-card-header {\n  background: #01bb0d;\n  color: #000;\n  padding: 6px;\n}\n.order ion-card-subtitle span {\n  float: right;\n  font-size: 10px;\n}\n.order ion-card-subtitle {\n  color: #fff;\n  font-size: 12px;\n}\n.order ion-card-title {\n  font-size: 13px;\n  font-weight: 600;\n  color: #fff;\n}\n.order {\n  background: #fafafa;\n}\n.order ion-card-content {\n  padding: 6px;\n}\n.address ion-icon {\n  font-size: 22px;\n  color: #fff;\n  background: #0f7b05;\n  border-radius: 50px;\n  padding: 4px;\n  margin-top: 28%;\n}\n.address p {\n  font-size: 12px;\n}\n.address p b {\n  font-size: 14px;\n  font-weight: 500;\n}\n.price ion-col {\n  font-size: 12px;\n  color: #ff304c;\n  font-weight: 700;\n}\n.price {\n  border-top: 1px solid #e8e8e8;\n}\n.spinner {\n  position: absolute;\n  top: 50%;\n  left: 45%;\n}\n.spinner ion-spinner {\n  width: 50px;\n  height: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWJvdXQvRjpcXFVzZXJzXFxBeWFuXFxEcm9wYm94XFxuZXcgYWxpYmFiYSBkZWxpdmVyeSBhcHAvc3JjXFxhcHBcXGFib3V0XFxhYm91dC5wYWdlLnNjc3MiLCJzcmMvYXBwL2Fib3V0L2Fib3V0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0FDQ0Y7QURBRTtFQUNFLGtCQUFBO0FDRUo7QURDQTtFQUFNLFdBQUE7RUFBYSxhQUFBO0VBQWUsZUFBQTtBQ0tsQztBREpBO0VBQXdCLG1CQUFBO0VBQXFCLFdBQUE7RUFBYSxZQUFBO0FDVTFEO0FEVEE7RUFBK0IsWUFBQTtFQUFjLGVBQUE7QUNjN0M7QURiQTtFQUEwQixXQUFBO0VBQWEsZUFBQTtBQ2tCdkM7QURqQkE7RUFBdUIsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixXQUFBO0FDdUIxRDtBRHRCQTtFQUFRLG1CQUFBO0FDMEJSO0FEekJBO0VBQXlCLFlBQUE7QUM2QnpCO0FENUJBO0VBQW1CLGVBQUE7RUFBaUIsV0FBQTtFQUFhLG1CQUFBO0VBQXFCLG1CQUFBO0VBQXFCLFlBQUE7RUFBYyxlQUFBO0FDcUN6RztBRHBDQTtFQUFZLGVBQUE7QUN3Q1o7QUR2Q0E7RUFBYyxlQUFBO0VBQWlCLGdCQUFBO0FDNEMvQjtBRDNDQTtFQUFnQixlQUFBO0VBQWlCLGNBQUE7RUFBZ0IsZ0JBQUE7QUNpRGpEO0FEaERBO0VBQVEsNkJBQUE7QUNvRFI7QURuREE7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FDc0RGO0FEcERBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUN1REYiLCJmaWxlIjoic3JjL2FwcC9hYm91dC9hYm91dC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud3JhcC1zZWN0aW9ue1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAudGV4dC1zZWN0aW9ue1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxufVxyXG4ubWFweyB3aWR0aDogMTAwJTsgaGVpZ2h0OiAyNTBweDsgcG9zaXRpb246IGZpeGVkOyB9XHJcbi5vcmRlciBpb24tY2FyZC1oZWFkZXJ7IGJhY2tncm91bmQ6ICMwMWJiMGQ7IGNvbG9yOiAjMDAwOyBwYWRkaW5nOiA2cHg7IH1cclxuLm9yZGVyIGlvbi1jYXJkLXN1YnRpdGxlIHNwYW57IGZsb2F0OiByaWdodDsgZm9udC1zaXplOiAxMHB4OyB9XHJcbi5vcmRlciBpb24tY2FyZC1zdWJ0aXRsZXsgY29sb3I6ICNmZmY7IGZvbnQtc2l6ZTogMTJweDsgfVxyXG4ub3JkZXIgaW9uLWNhcmQtdGl0bGV7IGZvbnQtc2l6ZTogMTNweDsgZm9udC13ZWlnaHQ6IDYwMDsgY29sb3I6ICNmZmY7IH1cclxuLm9yZGVyeyBiYWNrZ3JvdW5kOiAjZmFmYWZhOyB9XHJcbi5vcmRlciBpb24tY2FyZC1jb250ZW50eyBwYWRkaW5nOiA2cHg7IH1cclxuLmFkZHJlc3MgaW9uLWljb257IGZvbnQtc2l6ZTogMjJweDsgY29sb3I6ICNmZmY7IGJhY2tncm91bmQ6ICMwZjdiMDU7IGJvcmRlci1yYWRpdXM6IDUwcHg7IHBhZGRpbmc6IDRweDsgbWFyZ2luLXRvcDogMjglOyB9XHJcbi5hZGRyZXNzIHB7IGZvbnQtc2l6ZTogMTJweDsgfVxyXG4uYWRkcmVzcyBwIGJ7IGZvbnQtc2l6ZTogMTRweDsgZm9udC13ZWlnaHQ6IDUwMDsgfVxyXG4ucHJpY2UgaW9uLWNvbHsgZm9udC1zaXplOiAxMnB4OyBjb2xvcjogI2ZmMzA0YzsgZm9udC13ZWlnaHQ6IDcwMDsgfVxyXG4ucHJpY2V7IGJvcmRlci10b3A6IDFweCBzb2xpZCAjZThlOGU4OyB9XHJcbi5zcGlubmVye1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDUwJTtcclxuICBsZWZ0OiA0NSU7XHJcbn1cclxuLnNwaW5uZXIgaW9uLXNwaW5uZXJ7XHJcbiAgd2lkdGg6IDUwcHg7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcbiIsIi53cmFwLXNlY3Rpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ud3JhcC1zZWN0aW9uIC50ZXh0LXNlY3Rpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5tYXAge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyNTBweDtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuXG4ub3JkZXIgaW9uLWNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogIzAxYmIwZDtcbiAgY29sb3I6ICMwMDA7XG4gIHBhZGRpbmc6IDZweDtcbn1cblxuLm9yZGVyIGlvbi1jYXJkLXN1YnRpdGxlIHNwYW4ge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLm9yZGVyIGlvbi1jYXJkLXN1YnRpdGxlIHtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm9yZGVyIGlvbi1jYXJkLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLm9yZGVyIHtcbiAgYmFja2dyb3VuZDogI2ZhZmFmYTtcbn1cblxuLm9yZGVyIGlvbi1jYXJkLWNvbnRlbnQge1xuICBwYWRkaW5nOiA2cHg7XG59XG5cbi5hZGRyZXNzIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZDogIzBmN2IwNTtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgcGFkZGluZzogNHB4O1xuICBtYXJnaW4tdG9wOiAyOCU7XG59XG5cbi5hZGRyZXNzIHAge1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5hZGRyZXNzIHAgYiB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLnByaWNlIGlvbi1jb2wge1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjZmYzMDRjO1xuICBmb250LXdlaWdodDogNzAwO1xufVxuXG4ucHJpY2Uge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2U4ZThlODtcbn1cblxuLnNwaW5uZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA0NSU7XG59XG5cbi5zcGlubmVyIGlvbi1zcGlubmVyIHtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogNTBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/about/about.page.ts":
/*!*************************************!*\
  !*** ./src/app/about/about.page.ts ***!
  \*************************************/
/*! exports provided: AboutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPage", function() { return AboutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/message-helper */ "./src/providers/message-helper.ts");
/* harmony import */ var src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/data/appstorage */ "./src/providers/data/appstorage.ts");
/* harmony import */ var src_utils_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/utils/constants */ "./src/utils/constants.ts");
/* harmony import */ var src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/providers/data/data */ "./src/providers/data/data.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");










let AboutPage = class AboutPage {
    constructor(navCtrl, msgHelper, loadingCtrl, platform, modalCtrl, actionSheetCtrl, storage, formBuilder, router, modalController, element, renderer, http, geolocation) {
        this.navCtrl = navCtrl;
        this.msgHelper = msgHelper;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.router = router;
        this.modalController = modalController;
        this.element = element;
        this.renderer = renderer;
        this.http = http;
        this.geolocation = geolocation;
        this.allOrderlists = [];
        this.start = 1;
        this.hasMoreData = true;
        this.passValues = false;
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID).then(userid => {
            this.userID = userid;
            console.log('orderPage userID', this.userID);
            this.getAllOrders();
            this.start = 1;
        });
    }
    ngOnInit() {
    }
    orderDetails(order) {
        this.router.navigate(['/product-details', { orderNo: order.OrderNo }]);
    }
    getAllOrders() {
        this.http.callApi('pendingorderlist', {
            'DriverId': this.userID,
            'OrderNo': '',
            'CustomerName': '',
            'CustomerPhone': '',
            'Page': '1',
            'Size': '10'
        }).then((data) => {
            if (data.status == src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                if (!this.http.isNullOrEmpty(data)) {
                    this.allOrderlists = data.data;
                    console.log('allOrderlists 1st 10--', this.allOrderlists);
                    this.passValues = true;
                }
            }
            else {
                this.msgHelper.presentAlertok('Failed', 'Please try again.');
            }
        });
    }
    doInfinite() {
        this.start += 1;
        // this.pageSize += 10;
        return new Promise((resolve) => {
            this.http.callApi('pendingorderlist', {
                'DriverId': this.userID,
                'OrderNo': '',
                'CustomerName': '',
                'CustomerPhone': '',
                'Page': this.start,
                'Size': '10'
            }).then((data) => {
                //loader.dismiss();
                let count = 0;
                if (!this.http.isNullOrEmpty(data)) {
                    console.log(data.data);
                    this.allOrderlists = [].concat(this.allOrderlists, data.data);
                    console.log('allOrderlists 2nd--', this.allOrderlists);
                }
                if (count > 0) {
                    resolve();
                }
                else {
                    this.hasMoreData = false;
                }
            }, (err) => {
                console.log(err);
                resolve();
            });
        });
    }
};
AboutPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"] },
    { type: src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"] }
];
AboutPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-about',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./about.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./about.page.scss */ "./src/app/about/about.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"], src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"]])
], AboutPage);



/***/ })

}]);
//# sourceMappingURL=about-about-module-es2015.js.map