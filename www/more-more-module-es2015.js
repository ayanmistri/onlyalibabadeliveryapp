(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["more-more-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/more/more.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/more/more.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>My Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"more\">\n\n  <div class=\"user\">\n    <!--<img src=\"assets/images/avt8.jpg\">-->\n    <!--<ion-icon name=\"image-outline\" style=\"font-size:5rem;\" *ngIf=\"base64Image == null\" (click)=\"openActionSheet()\"></ion-icon>-->\n    <img *ngIf=\"base64Image == null\" src=\"{{base64ImageNew}}\" (click)=\"openActionSheet()\"/>\n    <img *ngIf=\"base64Image != null\" src=\"{{base64Image}}\" (click)=\"openActionSheet()\"/>\n  </div>\n  <ion-list>\n    <form padding [formGroup]=\"contactForm\">\n\n      <ion-item>\n        <ion-label position=\"floating\">Name</ion-label>\n        <ion-input #fullName (change)=\"elementChanged(fullName)\" type=\"text\" formControlName=\"fullName\"\n                   [class.invalid]=\"!contactForm.controls.fullName.valid && (fullNameChanged || submitAttempt)\"></ion-input>\n      </ion-item>\n\n      <p style=\"color: #d33939; font-size:10px;\" *ngIf=\"!contactForm.controls.fullName.valid && contactForm.controls.fullName.touched\">* Please enter a valid name (least 8 characters)</p>\n\n      <ion-item>\n        <ion-label position=\"floating\">Email</ion-label>\n        <ion-input #email (change)=\"elementChanged(email)\" type=\"email\" formControlName=\"email\"\n                   [class.invalid]=\"!contactForm.controls.email.valid && (emailChanged || submitAttempt)\"></ion-input>\n      </ion-item>\n\n      <p style=\"color: #d33939; font-size:10px;\" *ngIf=\"!contactForm.controls.email.valid && contactForm.controls.email.touched\">* Please enter a valid email</p>\n\n      <ion-item>\n        <ion-label position=\"floating\">Address</ion-label>\n        <ion-textarea style=\"--padding-top: 0;\" #address (change)=\"elementChanged(address)\" type=\"text\" formControlName=\"address\"\n                      [class.invalid]=\"!contactForm.controls.address.valid && (addressChanged || submitAttempt)\"></ion-textarea>\n      </ion-item>\n\n\n      <p style=\"color: #d33939; font-size:10px;\" *ngIf=\"!contactForm.controls.address.valid && contactForm.controls.address.touched\">* Please enter a valid address (least 10 characters)</p>\n\n      <ion-item>\n        <ion-label position=\"floating\">Landmark</ion-label>\n        <ion-input #landmark (change)=\"elementChanged(landmark)\" type=\"text\" formControlName=\"landmark\"\n                   [class.invalid]=\"!contactForm.controls.landmark.valid && (landmarkChanged || submitAttempt)\"></ion-input>\n      </ion-item>\n\n      <p style=\"color: #d33939; font-size:10px;\" *ngIf=\"!contactForm.controls.landmark.valid && contactForm.controls.landmark.touched\">* Please enter a valid landmark</p>\n\n      <ion-item>\n        <ion-label position=\"floating\">Pin code</ion-label>\n        <ion-input #pincode (change)=\"elementChanged(pincode)\" type=\"number\" formControlName=\"pincode\"\n                   [class.invalid]=\"!contactForm.controls.pincode.valid && (pincodeChanged || submitAttempt)\"></ion-input>\n      </ion-item>\n\n      <p style=\"color: #d33939; font-size:10px;\" *ngIf=\"!contactForm.controls.pincode.valid && contactForm.controls.pincode.touched\">* Please enter a valid 6 digit pin code</p>\n\n      <div padding text-center>\n        <ion-button color=\"primary\" expand=\"block\" (click)=\"profileSave()\" [disabled]=\"!contactForm.valid\" block ion-button>Update Profile</ion-button>\n      </div>\n    </form>\n  </ion-list>\n\n</ion-content>\n\n<!--<ion-footer>-->\n  <!--<ion-button color=\"primary\" expand=\"block\" (click)=\"profileSave()\" [disabled]=\"!contactForm.valid\" block ion-button>Update Profile</ion-button>-->\n<!--</ion-footer>-->");

/***/ }),

/***/ "./src/app/more/more.module.ts":
/*!*************************************!*\
  !*** ./src/app/more/more.module.ts ***!
  \*************************************/
/*! exports provided: MorePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MorePageModule", function() { return MorePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _more_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./more.page */ "./src/app/more/more.page.ts");







const routes = [
    {
        path: '',
        component: _more_page__WEBPACK_IMPORTED_MODULE_6__["MorePage"]
    }
];
let MorePageModule = class MorePageModule {
};
MorePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_more_page__WEBPACK_IMPORTED_MODULE_6__["MorePage"]]
    })
], MorePageModule);



/***/ }),

/***/ "./src/app/more/more.page.scss":
/*!*************************************!*\
  !*** ./src/app/more/more.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".more ion-list ion-item {\n  --min-height: 30px;\n  margin-bottom: 5px;\n  font-size: 10px;\n  border-radius: 3px;\n}\n\n.more h4 {\n  margin-top: 0;\n}\n\n.more {\n  --background: #f0f0f0;\n}\n\n.more ion-list {\n  padding: 5px 16px 16px 16px;\n  background: inherit;\n}\n\n.user {\n  text-align: center;\n  margin-top: 18px;\n}\n\n.user img {\n  display: inline-block;\n  border-radius: 50px;\n  width: 100px;\n  height: 100px;\n  border: 3px solid #ccc;\n  padding: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9yZS9GOlxcVXNlcnNcXEF5YW5cXERyb3Bib3hcXG5ldyBhbGliYWJhIGRlbGl2ZXJ5IGFwcC9zcmNcXGFwcFxcbW9yZVxcbW9yZS5wYWdlLnNjc3MiLCJzcmMvYXBwL21vcmUvbW9yZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBeUIsa0JBQUE7RUFBb0Isa0JBQUE7RUFBb0IsZUFBQTtFQUFpQixrQkFBQTtBQ0tsRjs7QURKQTtFQUFVLGFBQUE7QUNRVjs7QURQQTtFQUFPLHFCQUFBO0FDV1A7O0FEVkE7RUFBZ0IsMkJBQUE7RUFBNEIsbUJBQUE7QUNlNUM7O0FEZEE7RUFBTyxrQkFBQTtFQUFvQixnQkFBQTtBQ21CM0I7O0FEbEJBO0VBQVcscUJBQUE7RUFBdUIsbUJBQUE7RUFBcUIsWUFBQTtFQUFjLGFBQUE7RUFBZSxzQkFBQTtFQUF3QixZQUFBO0FDMkI1RyIsImZpbGUiOiJzcmMvYXBwL21vcmUvbW9yZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubW9yZSBpb24tbGlzdCBpb24taXRlbXsgLS1taW4taGVpZ2h0OiAzMHB4OyBtYXJnaW4tYm90dG9tOiA1cHg7IGZvbnQtc2l6ZTogMTBweDsgYm9yZGVyLXJhZGl1czogM3B4OyB9XHJcbi5tb3JlIGg0eyBtYXJnaW4tdG9wOiAwOyB9XHJcbi5tb3JleyAtLWJhY2tncm91bmQ6ICNmMGYwZjA7IH1cclxuLm1vcmUgaW9uLWxpc3R7IHBhZGRpbmc6NXB4IDE2cHggMTZweCAxNnB4OyBiYWNrZ3JvdW5kOiBpbmhlcml0OyB9XHJcbi51c2VyeyB0ZXh0LWFsaWduOiBjZW50ZXI7IG1hcmdpbi10b3A6IDE4cHg7IH1cclxuLnVzZXIgaW1neyBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGJvcmRlci1yYWRpdXM6IDUwcHg7IHdpZHRoOiAxMDBweDsgaGVpZ2h0OiAxMDBweDsgYm9yZGVyOiAzcHggc29saWQgI2NjYzsgcGFkZGluZzogM3B4OyB9XHJcblxyXG5cclxuXHJcblxyXG5cclxuIiwiLm1vcmUgaW9uLWxpc3QgaW9uLWl0ZW0ge1xuICAtLW1pbi1oZWlnaHQ6IDMwcHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgZm9udC1zaXplOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbi5tb3JlIGg0IHtcbiAgbWFyZ2luLXRvcDogMDtcbn1cblxuLm1vcmUge1xuICAtLWJhY2tncm91bmQ6ICNmMGYwZjA7XG59XG5cbi5tb3JlIGlvbi1saXN0IHtcbiAgcGFkZGluZzogNXB4IDE2cHggMTZweCAxNnB4O1xuICBiYWNrZ3JvdW5kOiBpbmhlcml0O1xufVxuXG4udXNlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMThweDtcbn1cblxuLnVzZXIgaW1nIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIGJvcmRlcjogM3B4IHNvbGlkICNjY2M7XG4gIHBhZGRpbmc6IDNweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/more/more.page.ts":
/*!***********************************!*\
  !*** ./src/app/more/more.page.ts ***!
  \***********************************/
/*! exports provided: MorePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MorePage", function() { return MorePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/message-helper */ "./src/providers/message-helper.ts");
/* harmony import */ var src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/data/appstorage */ "./src/providers/data/appstorage.ts");
/* harmony import */ var src_utils_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/utils/constants */ "./src/utils/constants.ts");
/* harmony import */ var src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/providers/data/data */ "./src/providers/data/data.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");











let MorePage = class MorePage {
    constructor(alertController, loadingController, router, formBuilder, msgHelper, storage, http, navCtrl, camera) {
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.router = router;
        this.formBuilder = formBuilder;
        this.msgHelper = msgHelper;
        this.storage = storage;
        this.http = http;
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.base64Image = null;
        this.submitAttempt = false;
        this.fullNameChanged = false;
        this.emailChanged = false;
        this.addressChanged = false;
        this.landmarkChanged = false;
        this.pincodeChanged = false;
        this.contactForm = this.formBuilder.group({
            fullName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(8)]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].EMAIL_REGEX)]),
            // phone: new FormControl ('', [Validators.required, Validators.minLength(10)]),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(10)]),
            landmark: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            pincode: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].PIN_REGEX)])
        });
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_NAME).then(username => {
            this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].EMAIL).then(email => {
                // this.storage.getValue(Constants.MOBILE).then(phone => {
                this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].ADDRESS).then(address => {
                    this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].Landmark).then(landmark => {
                        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].PINCODE).then(pincode => {
                            this.contactForm = this.formBuilder.group({
                                fullName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](username, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(8)]),
                                email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](email, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].EMAIL_REGEX)]),
                                // phone:  new FormControl (phone, [Validators.required, Validators.minLength(10)]),
                                address: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](address, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(10)]),
                                landmark: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](landmark, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
                                pincode: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](pincode, [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].PIN_REGEX)])
                            });
                        });
                    });
                });
                // });
            });
        });
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].IMAGE).then(userimg => {
            this.base64ImageNew = userimg;
        });
    }
    ngOnInit() {
    }
    openActionSheet() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = document.createElement('ion-action-sheet');
            actionSheet.header = 'Please select';
            actionSheet.cssClass = 'my-custom-class';
            actionSheet.buttons = [{
                    text: 'Camera',
                    // role: 'destructive',
                    icon: 'camera',
                    handler: () => {
                        this.takePhoto();
                    }
                }, {
                    text: 'Gallery',
                    icon: 'images',
                    handler: () => {
                        this.openGallery();
                    }
                }, {
                    text: 'Cancel',
                    icon: 'close',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }];
            document.body.appendChild(actionSheet);
            return actionSheet.present();
        });
    }
    takePhoto() {
        const cameraOptions = {
            quality: 50,
            targetHeight: 350,
            targetWidth: 350,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            cameraDirection: 1
        };
        this.camera.getPicture(cameraOptions).then((imageData) => {
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log("img path", this.base64Image);
            this.base64ImageNew = imageData;
        }, (err) => {
            console.log(err);
        });
    }
    openGallery() {
        this.camera.getPicture({
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL
        }).then((imageData) => {
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log("img path", this.base64Image);
            this.base64ImageNew = imageData;
        }, (err) => {
            console.log(err);
        });
    }
    profileSave() {
        this.msgHelper.presentLoading();
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID).then(driverId => {
            this.http.callApi("profileupdate", {
                "driverId": driverId,
                "name": this.contactForm.value.fullName,
                "email": this.contactForm.value.email,
                "address": this.contactForm.value.address,
                "landmark": this.contactForm.value.landmark,
                "pincode": this.contactForm.value.pincode,
                "myFile": this.base64Image
            }).then((data) => {
                this.msgHelper.dismiss();
                if (data.status === src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                    this.router.navigate(['/menu/home']);
                    this.msgHelper.presentToast("Profile updated successfully");
                }
                else {
                    this.msgHelper.presentAlert("Failed", "Please try again.");
                }
            });
        });
    }
};
MorePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"] },
    { type: src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"] },
    { type: src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"] }
];
MorePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-more',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./more.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/more/more.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./more.page.scss */ "./src/app/more/more.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"], src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"], src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"]])
], MorePage);



/***/ })

}]);
//# sourceMappingURL=more-more-module-es2015.js.map