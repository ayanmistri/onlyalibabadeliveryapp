(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["menu-menu-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMenuMenuPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-split-pane>\n  <ion-menu contentId=\"content\">\n    <ion-header class=\"menu\">\n      <ion-toolbar color=\"primary\">\n        <ion-title><img src=\"assets/images/avt8.jpg\">\n          <p>{{userName}}<br>\n            <small>{{'+91 '+phNo}}</small></p>\n        </ion-title>\n      </ion-toolbar>\n    </ion-header>\n\n    <ion-content class=\"menu1\">\n      <ion-list lines=\"none\">\n        <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let p of pages\">\n          <ion-item [routerLink]=\"p.url\" routerDirection=\"root\" [class.active-item]=\"selectedPath.startsWith(p.url)\">\n            <ion-icon color=\"primary\" name=\"{{p.icon}}\"></ion-icon>\n            <ion-label>\n              {{ p.title }}\n            </ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n      </ion-list>\n    </ion-content>\n    <ion-footer class=\"menu1\">\n      <ion-list lines=\"none\">\n        <ion-item (click)=\"logOut()\">\n          <ion-icon color=\"primary\" name=\"log-out\" style=\"margin-right: 8px;\"></ion-icon>\n          <ion-label>\n            Log Out\n          </ion-label>\n        </ion-item>\n      </ion-list>\n    </ion-footer>\n\n  </ion-menu>\n\n  <ion-router-outlet id=\"content\" main></ion-router-outlet>\n</ion-split-pane>";
    /***/
  },

  /***/
  "./src/app/menu/menu.module.ts":
  /*!*************************************!*\
    !*** ./src/app/menu/menu.module.ts ***!
    \*************************************/

  /*! exports provided: MenuPageModule */

  /***/
  function srcAppMenuMenuModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MenuPageModule", function () {
      return MenuPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./menu.page */
    "./src/app/menu/menu.page.ts"); // const routes: Routes = [
    //     {
    //         path: '',
    //         component: MenuPage,
    //         children: [
    //             {
    //                 path: 'dashboard',
    //                 loadChildren: '../dashboard/dashboard.module#DashboardPageModule',
    //
    //             }
    //             // {
    //             //   path: 'cart',
    //             //   loadChildren: '../cart/cart.module#CartPageModule',
    //             //
    //             // }
    //
    //         ]
    //     }
    // ];


    const routes = [{
      path: '',
      component: _menu_page__WEBPACK_IMPORTED_MODULE_6__["MenuPage"],
      children: [{
        path: 'home',
        loadChildren: () => __webpack_require__.e(
        /*! import() | home-home-module */
        "home-home-module").then(__webpack_require__.bind(null,
        /*! ../home/home.module */
        "./src/app/home/home.module.ts")).then(m => m.HomePageModule)
      }, {
        path: 'about',
        loadChildren: () => __webpack_require__.e(
        /*! import() | about-about-module */
        "about-about-module").then(__webpack_require__.bind(null,
        /*! ../about/about.module */
        "./src/app/about/about.module.ts")).then(m => m.AboutPageModule)
      }, {
        path: 'contact',
        loadChildren: () => __webpack_require__.e(
        /*! import() | contact-contact-module */
        "contact-contact-module").then(__webpack_require__.bind(null,
        /*! ../contact/contact.module */
        "./src/app/contact/contact.module.ts")).then(m => m.ContactPageModule)
      }, {
        path: 'more',
        loadChildren: () => __webpack_require__.e(
        /*! import() | more-more-module */
        "more-more-module").then(__webpack_require__.bind(null,
        /*! ../more/more.module */
        "./src/app/more/more.module.ts")).then(m => m.MorePageModule)
      }]
    }];
    let MenuPageModule = class MenuPageModule {};
    MenuPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_menu_page__WEBPACK_IMPORTED_MODULE_6__["MenuPage"]]
    })], MenuPageModule);
    /***/
  },

  /***/
  "./src/app/menu/menu.page.scss":
  /*!*************************************!*\
    !*** ./src/app/menu/menu.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppMenuMenuPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".active-item {\n  border-left: 8px solid var(--ion-color-primary);\n}\n\n.menu img {\n  width: 35px;\n  height: 35px;\n  display: inline-block;\n  border-radius: 50px;\n  margin-right: 12px;\n  border: 1px solid rgba(255, 255, 255, 0.6);\n  padding: 2px;\n}\n\n.menu ion-title {\n  padding-left: 16px;\n}\n\n.menu p {\n  display: inline-block;\n  margin: 0;\n  font-size: 14px;\n}\n\n.menu1 {\n  --background: #1F2024;\n}\n\n.menu1 ion-list {\n  background: inherit;\n  padding: 0;\n}\n\n.menu1 ion-list ion-item {\n  --background: inherit;\n}\n\n.menu1 ion-list ion-item ion-label {\n  font-size: 13px;\n  color: #999;\n}\n\nion-footer {\n  --background: #333 !important;\n}\n\n.menu1 ion-icon {\n  font-size: 1.4rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVudS9GOlxcVXNlcnNcXEF5YW5cXERyb3Bib3hcXG5ldyBhbGliYWJhIGRlbGl2ZXJ5IGFwcC9zcmNcXGFwcFxcbWVudVxcbWVudS5wYWdlLnNjc3MiLCJzcmMvYXBwL21lbnUvbWVudS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwrQ0FBQTtBQ0NKOztBRENBO0VBQVcsV0FBQTtFQUFhLFlBQUE7RUFBYyxxQkFBQTtFQUF1QixtQkFBQTtFQUFxQixrQkFBQTtFQUFvQiwwQ0FBQTtFQUF5QyxZQUFBO0FDUy9JOztBRFJBO0VBQWlCLGtCQUFBO0FDWWpCOztBRFhBO0VBQVMscUJBQUE7RUFBdUIsU0FBQTtFQUFXLGVBQUE7QUNpQjNDOztBRGhCQTtFQUFRLHFCQUFBO0FDb0JSOztBRG5CQTtFQUFpQixtQkFBQTtFQUFxQixVQUFBO0FDd0J0Qzs7QUR2QkE7RUFBMEIscUJBQUE7QUMyQjFCOztBRDFCQTtFQUFvQyxlQUFBO0VBQWlCLFdBQUE7QUMrQnJEOztBRDlCQTtFQUFZLDZCQUFBO0FDa0NaOztBRGpDQTtFQUFpQixpQkFBQTtBQ3FDakIiLCJmaWxlIjoic3JjL2FwcC9tZW51L21lbnUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFjdGl2ZS1pdGVtIHtcclxuICAgIGJvcmRlci1sZWZ0OiA4cHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcbi5tZW51IGltZ3sgd2lkdGg6IDM1cHg7IGhlaWdodDogMzVweDsgZGlzcGxheTogaW5saW5lLWJsb2NrOyBib3JkZXItcmFkaXVzOiA1MHB4OyBtYXJnaW4tcmlnaHQ6IDEycHg7IGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjU1LDI1NSwyNTUsMC42KTsgcGFkZGluZzogMnB4OyB9XHJcbi5tZW51IGlvbi10aXRsZXsgcGFkZGluZy1sZWZ0OiAxNnB4OyB9XHJcbi5tZW51IHB7IGRpc3BsYXk6IGlubGluZS1ibG9jazsgbWFyZ2luOiAwOyBmb250LXNpemU6IDE0cHg7IH1cclxuLm1lbnUxeyAtLWJhY2tncm91bmQ6ICMxRjIwMjQ7IH1cclxuLm1lbnUxIGlvbi1saXN0eyBiYWNrZ3JvdW5kOiBpbmhlcml0OyBwYWRkaW5nOjA7IH1cclxuLm1lbnUxIGlvbi1saXN0IGlvbi1pdGVteyAtLWJhY2tncm91bmQ6IGluaGVyaXQ7IH1cclxuLm1lbnUxIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbHsgZm9udC1zaXplOiAxM3B4OyBjb2xvcjogIzk5OTsgfVxyXG5pb24tZm9vdGVyeyAtLWJhY2tncm91bmQ6ICMzMzMgIWltcG9ydGFudDsgfVxyXG4ubWVudTEgaW9uLWljb257IGZvbnQtc2l6ZTogMS40cmVtOyB9XHJcblxyXG4iLCIuYWN0aXZlLWl0ZW0ge1xuICBib3JkZXItbGVmdDogOHB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuLm1lbnUgaW1nIHtcbiAgd2lkdGg6IDM1cHg7XG4gIGhlaWdodDogMzVweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEycHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcbiAgcGFkZGluZzogMnB4O1xufVxuXG4ubWVudSBpb24tdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDE2cHg7XG59XG5cbi5tZW51IHAge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogMDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4ubWVudTEge1xuICAtLWJhY2tncm91bmQ6ICMxRjIwMjQ7XG59XG5cbi5tZW51MSBpb24tbGlzdCB7XG4gIGJhY2tncm91bmQ6IGluaGVyaXQ7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tZW51MSBpb24tbGlzdCBpb24taXRlbSB7XG4gIC0tYmFja2dyb3VuZDogaW5oZXJpdDtcbn1cblxuLm1lbnUxIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgY29sb3I6ICM5OTk7XG59XG5cbmlvbi1mb290ZXIge1xuICAtLWJhY2tncm91bmQ6ICMzMzMgIWltcG9ydGFudDtcbn1cblxuLm1lbnUxIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAxLjRyZW07XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/menu/menu.page.ts":
  /*!***********************************!*\
    !*** ./src/app/menu/menu.page.ts ***!
    \***********************************/

  /*! exports provided: MenuPage */

  /***/
  function srcAppMenuMenuPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MenuPage", function () {
      return MenuPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/data/appstorage */
    "./src/providers/data/appstorage.ts");
    /* harmony import */


    var src_utils_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/utils/constants */
    "./src/utils/constants.ts");

    let MenuPage = class MenuPage {
      constructor(alertCtrl, loadingCtrl, toastCtrl, menuCtrl, storage, router, platform) {
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.menuCtrl = menuCtrl;
        this.storage = storage;
        this.router = router;
        this.platform = platform;
        this.selectedPath = '';
        this.pages = [{
          title: 'Home',
          url: '/menu/home',
          icon: 'home'
        }, {
          title: 'Order',
          url: '/menu/about',
          icon: 'document'
        }, {
          title: 'Past Order History',
          url: '/menu/contact',
          icon: 'copy'
        }, {
          title: 'My Profile',
          url: '/menu/more',
          icon: 'person'
        }];
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_5__["Constants"].USER_NAME).then(name => {
          this.userName = name;
        });
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_5__["Constants"].MOBILE).then(phno => {
          this.phNo = phno;
        }); // this.router.events.subscribe((event: RouterEvent) => {
        //   if (event && event.url) {
        //     this.selectedPath = event.url;
        //   }
        // });
      }

      ngOnInit() {}

      logOut() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
          this.menuCtrl.close();
          const alert = yield this.alertCtrl.create({
            header: 'Log Out',
            message: 'Are you sure to logout?',
            buttons: [{
              text: "No"
            }, {
              text: "Yes",
              handler: () => {
                this.storage.storage.clear();
                this.router.navigate(['/login']);
              }
            }]
          });
          yield alert.present();
        });
      }

    };

    MenuPage.ctorParameters = () => [{
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]
    }, {
      type: src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_4__["AppStorage"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
    }, {
      type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
    }];

    MenuPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-menu',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./menu.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./menu.page.scss */
      "./src/app/menu/menu.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"], src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_4__["AppStorage"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])], MenuPage);
    /***/
  }
}]);
//# sourceMappingURL=menu-menu-module-es5.js.map