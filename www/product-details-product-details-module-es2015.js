(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-details-product-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/product-details/product-details.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/product-details/product-details.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-back-button slot=\"start\">\n    </ion-back-button>\n    <ion-title>\n      Order Details\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content common-parallax [scrollEvents]=\"true\">\n  <div class=\"mapProduct\" id=\"mapProduct\"></div>\n  <div style=\"margin-top: 0%;\">\n    <ion-card class=\"ion-text-wrap order\">\n      <ion-row class=\"call\">\n        <ion-col size=\"6\" text-left>\n          <ion-button size=\"small\" shape=\"round\" (click)=\"viewPh(userPhone)\">\n            <ion-icon slot=\"start\" name=\"call\"></ion-icon> Call User\n          </ion-button>\n        </ion-col>\n        <ion-col size=\"6\" text-right>\n          <ion-button shape=\"round\" color=\"success\" size=\"small\" (click)=\"viewMapOn()\">\n            <ion-icon slot=\"start\" name=\"navigate\"></ion-icon> Direction\n          </ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-card-content>\n        <ion-row class=\"address\">\n          <ion-col size=\"2\">\n            <ion-icon name=\"navigate\"></ion-icon>\n          </ion-col>\n          <ion-col size=\"10\">\n            <ion-note>{{'Customer Name: '+userCusName}}</ion-note>\n            <p><b>{{userAddressName}}</b><br>\n              {{userAddress}}<br>\n              Pin- {{userPincode}}</p>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n      <ion-card-header>\n        <ion-card-subtitle>Order Id: {{userOrderNo}} <span>{{userOrderDate}} | {{userOrderTime}}</span></ion-card-subtitle>\n        <ion-card-title>{{userOrderDetail}}</ion-card-title>\n        <ion-row class=\"price\">\n          <ion-col size=\"4\" text-left>\n            Total: {{'₹'+userOrderTotal}}<br>\n            <small>Tip: ₹20</small>\n          </ion-col>\n          <ion-col size=\"8\" text-right>\n            {{'Payment Mode: '+userPaymentMode}}\n          </ion-col>\n        </ion-row>\n      </ion-card-header>\n    </ion-card>\n  </div>\n\n</ion-content>\n\n<ion-footer>\n  <ion-row class=\"call\">\n    <!--<ion-col size=\"6\" text-left>-->\n      <!--<ion-button color=\"tertiary\">-->\n        <!--<ion-icon slot=\"start\" name=\"download\"></ion-icon> Pick Up-->\n      <!--</ion-button>-->\n    <!--</ion-col>-->\n    <ion-col size=\"12\" text-center>\n      <ion-button color=\"success\" expand=\"full\" (click)=\"viewDelivery()\">\n        <ion-icon slot=\"start\" name=\"home\"></ion-icon> Delivery\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>");

/***/ }),

/***/ "./src/app/product-details/product-details.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/product-details/product-details.module.ts ***!
  \***********************************************************/
/*! exports provided: ProductDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsPageModule", function() { return ProductDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _product_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product-details.page */ "./src/app/product-details/product-details.page.ts");







const routes = [
    {
        path: '',
        component: _product_details_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailsPage"]
    }
];
let ProductDetailsPageModule = class ProductDetailsPageModule {
};
ProductDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_product_details_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailsPage"]]
    })
], ProductDetailsPageModule);



/***/ }),

/***/ "./src/app/product-details/product-details.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/product-details/product-details.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrap-section {\n  position: relative;\n}\n.wrap-section .text-section {\n  position: relative;\n}\n.mapProduct {\n  width: 100%;\n  height: 200px;\n  position: fixed;\n}\n.details {\n  background: #fff;\n}\n.call ion-button {\n  --box-shadow: inherit;\n}\n.call {\n  background: #fff;\n}\n.order ion-card-header {\n  background: #FFF;\n  color: #000;\n  padding: 12px;\n}\n.order ion-card-subtitle span {\n  float: right;\n  font-size: 10px;\n}\n.order ion-card-subtitle {\n  color: #000;\n  font-size: 12px;\n}\n.order ion-card-title {\n  font-size: 13px;\n  font-weight: 400;\n}\n.order {\n  background: #fafafa;\n}\n.order ion-card-content {\n  padding: 6px;\n  background: #f0f0f0;\n}\n.address ion-icon {\n  font-size: 22px;\n  color: #fff;\n  background: #0f7b05;\n  border-radius: 50px;\n  padding: 4px;\n  margin-top: 50%;\n}\n.address p {\n  font-size: 12px;\n}\n.address p b {\n  font-size: 14px;\n  font-weight: 500;\n}\n.price ion-col {\n  font-size: 12px;\n  color: #fff;\n  font-weight: 700;\n}\n.price {\n  margin-top: 8px;\n  background: #e0c200;\n  padding: 5px;\n}\nion-footer ion-col {\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdC1kZXRhaWxzL0Y6XFxVc2Vyc1xcQXlhblxcRHJvcGJveFxcbmV3IGFsaWJhYmEgZGVsaXZlcnkgYXBwL3NyY1xcYXBwXFxwcm9kdWN0LWRldGFpbHNcXHByb2R1Y3QtZGV0YWlscy5wYWdlLnNjc3MiLCJzcmMvYXBwL3Byb2R1Y3QtZGV0YWlscy9wcm9kdWN0LWRldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7QUNDRjtBREFFO0VBQ0Usa0JBQUE7QUNFSjtBRENBO0VBQWEsV0FBQTtFQUFhLGFBQUE7RUFBZSxlQUFBO0FDS3pDO0FESkE7RUFBVSxnQkFBQTtBQ1FWO0FEUEE7RUFBa0IscUJBQUE7QUNXbEI7QURWQTtFQUFPLGdCQUFBO0FDY1A7QURiQTtFQUF3QixnQkFBQTtFQUFrQixXQUFBO0VBQWEsYUFBQTtBQ21CdkQ7QURsQkE7RUFBK0IsWUFBQTtFQUFjLGVBQUE7QUN1QjdDO0FEdEJBO0VBQTBCLFdBQUE7RUFBYSxlQUFBO0FDMkJ2QztBRDFCQTtFQUF1QixlQUFBO0VBQWlCLGdCQUFBO0FDK0J4QztBRDlCQTtFQUFRLG1CQUFBO0FDa0NSO0FEakNBO0VBQXlCLFlBQUE7RUFBYyxtQkFBQTtBQ3NDdkM7QURyQ0E7RUFBbUIsZUFBQTtFQUFpQixXQUFBO0VBQWEsbUJBQUE7RUFBcUIsbUJBQUE7RUFBcUIsWUFBQTtFQUFjLGVBQUE7QUM4Q3pHO0FEN0NBO0VBQVksZUFBQTtBQ2lEWjtBRGhEQTtFQUFjLGVBQUE7RUFBaUIsZ0JBQUE7QUNxRC9CO0FEcERBO0VBQWdCLGVBQUE7RUFBaUIsV0FBQTtFQUFhLGdCQUFBO0FDMEQ5QztBRHpEQTtFQUFRLGVBQUE7RUFBaUIsbUJBQUE7RUFBcUIsWUFBQTtBQytEOUM7QUQ5REE7RUFBb0IsU0FBQTtBQ2tFcEIiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0LWRldGFpbHMvcHJvZHVjdC1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53cmFwLXNlY3Rpb257XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC50ZXh0LXNlY3Rpb257XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG59XHJcbi5tYXBQcm9kdWN0eyB3aWR0aDogMTAwJTsgaGVpZ2h0OiAyMDBweDsgcG9zaXRpb246IGZpeGVkOyB9XHJcbi5kZXRhaWxzeyBiYWNrZ3JvdW5kOiAjZmZmOyB9XHJcbi5jYWxsIGlvbi1idXR0b257IC0tYm94LXNoYWRvdzogaW5oZXJpdDsgfVxyXG4uY2FsbHsgYmFja2dyb3VuZDogI2ZmZjsgfVxyXG4ub3JkZXIgaW9uLWNhcmQtaGVhZGVyeyBiYWNrZ3JvdW5kOiAjRkZGOyBjb2xvcjogIzAwMDsgcGFkZGluZzogMTJweDsgfVxyXG4ub3JkZXIgaW9uLWNhcmQtc3VidGl0bGUgc3BhbnsgZmxvYXQ6IHJpZ2h0OyBmb250LXNpemU6IDEwcHg7IH1cclxuLm9yZGVyIGlvbi1jYXJkLXN1YnRpdGxleyBjb2xvcjogIzAwMDsgZm9udC1zaXplOiAxMnB4OyB9XHJcbi5vcmRlciBpb24tY2FyZC10aXRsZXsgZm9udC1zaXplOiAxM3B4OyBmb250LXdlaWdodDogNDAwOyB9XHJcbi5vcmRlcnsgYmFja2dyb3VuZDogI2ZhZmFmYTsgfVxyXG4ub3JkZXIgaW9uLWNhcmQtY29udGVudHsgcGFkZGluZzogNnB4OyBiYWNrZ3JvdW5kOiAjZjBmMGYwOyB9XHJcbi5hZGRyZXNzIGlvbi1pY29ueyBmb250LXNpemU6IDIycHg7IGNvbG9yOiAjZmZmOyBiYWNrZ3JvdW5kOiAjMGY3YjA1OyBib3JkZXItcmFkaXVzOiA1MHB4OyBwYWRkaW5nOiA0cHg7IG1hcmdpbi10b3A6IDUwJTsgfVxyXG4uYWRkcmVzcyBweyBmb250LXNpemU6IDEycHg7IH1cclxuLmFkZHJlc3MgcCBieyBmb250LXNpemU6IDE0cHg7IGZvbnQtd2VpZ2h0OiA1MDA7IH1cclxuLnByaWNlIGlvbi1jb2x7IGZvbnQtc2l6ZTogMTJweDsgY29sb3I6ICNmZmY7IGZvbnQtd2VpZ2h0OiA3MDA7IH1cclxuLnByaWNleyBtYXJnaW4tdG9wOiA4cHg7IGJhY2tncm91bmQ6ICNlMGMyMDA7IHBhZGRpbmc6IDVweDsgfVxyXG5pb24tZm9vdGVyIGlvbi1jb2x7IG1hcmdpbjogMDsgfVxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbiIsIi53cmFwLXNlY3Rpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ud3JhcC1zZWN0aW9uIC50ZXh0LXNlY3Rpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5tYXBQcm9kdWN0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjAwcHg7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbn1cblxuLmRldGFpbHMge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4uY2FsbCBpb24tYnV0dG9uIHtcbiAgLS1ib3gtc2hhZG93OiBpbmhlcml0O1xufVxuXG4uY2FsbCB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5cbi5vcmRlciBpb24tY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjRkZGO1xuICBjb2xvcjogIzAwMDtcbiAgcGFkZGluZzogMTJweDtcbn1cblxuLm9yZGVyIGlvbi1jYXJkLXN1YnRpdGxlIHNwYW4ge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLm9yZGVyIGlvbi1jYXJkLXN1YnRpdGxlIHtcbiAgY29sb3I6ICMwMDA7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLm9yZGVyIGlvbi1jYXJkLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG4ub3JkZXIge1xuICBiYWNrZ3JvdW5kOiAjZmFmYWZhO1xufVxuXG4ub3JkZXIgaW9uLWNhcmQtY29udGVudCB7XG4gIHBhZGRpbmc6IDZweDtcbiAgYmFja2dyb3VuZDogI2YwZjBmMDtcbn1cblxuLmFkZHJlc3MgaW9uLWljb24ge1xuICBmb250LXNpemU6IDIycHg7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiAjMGY3YjA1O1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICBwYWRkaW5nOiA0cHg7XG4gIG1hcmdpbi10b3A6IDUwJTtcbn1cblxuLmFkZHJlc3MgcCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmFkZHJlc3MgcCBiIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4ucHJpY2UgaW9uLWNvbCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi5wcmljZSB7XG4gIG1hcmdpbi10b3A6IDhweDtcbiAgYmFja2dyb3VuZDogI2UwYzIwMDtcbiAgcGFkZGluZzogNXB4O1xufVxuXG5pb24tZm9vdGVyIGlvbi1jb2wge1xuICBtYXJnaW46IDA7XG59Il19 */");

/***/ }),

/***/ "./src/app/product-details/product-details.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/product-details/product-details.page.ts ***!
  \*********************************************************/
/*! exports provided: ProductDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsPage", function() { return ProductDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/message-helper */ "./src/providers/message-helper.ts");
/* harmony import */ var src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/data/appstorage */ "./src/providers/data/appstorage.ts");
/* harmony import */ var src_utils_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/utils/constants */ "./src/utils/constants.ts");
/* harmony import */ var src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/providers/data/data */ "./src/providers/data/data.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "./node_modules/@ionic-native/call-number/ngx/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");












let ProductDetailsPage = class ProductDetailsPage {
    constructor(navCtrl, msgHelper, loadingCtrl, platform, modalCtrl, actionSheetCtrl, storage, formBuilder, router, renderer, http, geolocation, activeRouter, callNumber, alertController, httpp) {
        this.navCtrl = navCtrl;
        this.msgHelper = msgHelper;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.router = router;
        this.renderer = renderer;
        this.http = http;
        this.geolocation = geolocation;
        this.activeRouter = activeRouter;
        this.callNumber = callNumber;
        this.alertController = alertController;
        this.httpp = httpp;
        this.orderNo = this.activeRouter.snapshot.paramMap.get('orderNo');
        this.storage.getValue(src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].USER_ID).then(userid => {
            this.userID = userid;
            console.log('ProductDetailsPage userID', this.userID);
            this.getOrderDetails();
        });
    }
    ngOnInit() {
    }
    getOrderDetails() {
        this.msgHelper.presentLoading();
        this.platform.ready().then(() => {
            this.http.callApi('orderdetailbyid', {
                'OrderId': this.orderNo,
            }).then((data) => {
                this.msgHelper.dismiss();
                if (data.status == src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                    this.userAddress = data.data[0].Address;
                    this.userAddressName = data.data[0].AddressName;
                    this.userLatitude = data.data[0].Latitude;
                    this.userLongitude = data.data[0].Longitude;
                    this.userOrderDate = data.data[0].OrderDate;
                    this.userOrderDetail = data.data[0].OrderDetail;
                    this.userOrderNo = data.data[0].OrderNo;
                    this.userOrderTime = data.data[0].OrderTime;
                    this.userOrderTotal = data.data[0].OrderTotal;
                    this.userPincode = data.data[0].Pincode;
                    this.userCusName = data.data[0].Name;
                    this.userPhone = data.data[0].Phone;
                    this.userPaymentMode = data.data[0].PaymentMode;
                    this.userCustomerId = data.data[0].CustomerId;
                    this.userRefCustomerId = data.data[0].RefCustomerId;
                    var lat = this.userLatitude;
                    var long = this.userLongitude;
                    var x = +this.userLatitude;
                    var y = +this.userLongitude;
                    const uluru = { lat: x, lng: y };
                    console.log('lat-long--', uluru);
                    const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpHeaders"]().set('Content-Type', 'text/plain; charset=utf-8');
                    var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + this.userLatitude + ',' + this.userLongitude + '&key=AIzaSyDSPMOX4Mr6bbq7Zkomw405OpYaC4CxAsg';
                    console.log(url);
                    this.httpp.get(url, { headers, responseType: 'text' }).subscribe(data => {
                        this.datas = JSON.parse(data);
                        console.log('data:', this.datas);
                        let address = this.datas.results[0].formatted_address;
                        this.area = this.datas.results[0].address_components[1].short_name;
                        console.log('My location:', address);
                        this.gpsaddress = address;
                    });
                    this.platform.ready().then(() => {
                        this.geolocation.getCurrentPosition().then((resp) => {
                            this.lat = resp.coords.latitude;
                            this.lon = resp.coords.longitude;
                            console.log('my latitude --', this.lat);
                            console.log('my longitude --', this.lon);
                            const uluruss = { lat: this.lat, lng: this.lon };
                            this.mapProduct = new google.maps.Map(document.getElementById('mapProduct'), {
                                zoom: 9,
                                center: uluruss,
                                disableDefaultUI: true,
                            });
                            var marker = new google.maps.Marker({
                                position: uluruss,
                                map: this.mapProduct,
                                label: {
                                    fontSize: "10pt",
                                    text: "You are here",
                                    fontWeight: "700",
                                    color: "#f00"
                                },
                                icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
                            });
                            var marker1 = new google.maps.Marker({
                                position: uluru,
                                map: this.mapProduct,
                                label: {
                                    fontSize: "10pt",
                                    text: "Delivery Location",
                                    fontWeight: "700",
                                    color: "#f00"
                                },
                                icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                            });
                        }).catch((error) => {
                            console.log('Error getting location', error);
                            // this.msgHelper.presentAlertok('Failed...', 'Do not fetch your location please check your gps connection.');
                        });
                    });
                    ////////////////////////////////
                }
                else {
                    this.msgHelper.presentAlertok('Failed', 'Please try again.');
                }
            });
        });
    }
    viewPh(userPhone) {
        console.log('userPhone---', userPhone);
        this.callNumber.callNumber(userPhone, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
    viewMapOn() {
        if (this.platform.is('android')) {
            // window.location.href = 'geo:,' + uluru;
            window.location.href = 'geo:' + '?q=' + this.gpsaddress;
        }
        else {
            var x = +this.userLatitude;
            var y = +this.userLongitude;
            const destination = { lat: x, lng: y };
            // const destination = '51.503391' + ',' + '-0.127630';
            window.open('maps://?q=' + destination + '_system');
            // window.location.href = 'maps://maps.apple.com/?q=' +  uluru;
        }
    }
    viewDelivery() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'OTP',
                message: 'Please enter the OTP sent to the customer order details.',
                inputs: [
                    {
                        name: 'otp',
                        placeholder: 'OTP',
                        type: 'number'
                    },
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    },
                    {
                        text: 'OK',
                        handler: data => {
                            if (data.otp) {
                                this.msgHelper.presentLoading();
                                this.http.callApi('orderupdate', {
                                    DriverId: this.userID,
                                    OrderId: this.userOrderNo,
                                    CustomerId: this.userCustomerId,
                                    RefCustomerId: this.userRefCustomerId,
                                    OrderTotal: this.userOrderTotal,
                                    OTP: data.otp
                                }).then((data) => {
                                    this.msgHelper.dismiss();
                                    if (data.status == src_utils_constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SUCCESS) {
                                        this.msgHelper.presentToast('Your order delivery has been completed.');
                                        this.router.navigate(['/menu/about']);
                                    }
                                    else {
                                        this.msgHelper.presentAlertok('Wrong OTP', 'The OTP you have entered is wrong. Please try again.');
                                        return false;
                                    }
                                });
                            }
                            else {
                                return false;
                            }
                        }
                    }
                ]
            });
            alert.present();
            let result = alert.onDidDismiss();
            console.log(result);
        });
    }
};
ProductDetailsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"] },
    { type: src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_10__["CallNumber"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClient"] }
];
ProductDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-details',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./product-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/product-details/product-details.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./product-details.page.scss */ "./src/app/product-details/product-details.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_message_helper__WEBPACK_IMPORTED_MODULE_5__["MessageHelper"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], src_providers_data_appstorage__WEBPACK_IMPORTED_MODULE_6__["AppStorage"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer"], src_providers_data_data__WEBPACK_IMPORTED_MODULE_8__["HttpProvider"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_10__["CallNumber"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClient"]])
], ProductDetailsPage);



/***/ })

}]);
//# sourceMappingURL=product-details-product-details-module-es2015.js.map